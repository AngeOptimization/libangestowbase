#include "schedulereader.h"

#include "bundle.h"
#include "rulesconverter.h"

#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>

#include <limits>
#include <cstdlib>

namespace ange {
namespace stowbase {

using schedule::Schedule;
using schedule::Call;

struct schedule_reader_private_t {
  QHash<QString, const Call*> call_map;
};

Schedule* ScheduleReader::readSchedule(QIODevice* device)  {
  Bundle bundle;
  bundle.read(device);
  Schedule* rv = readSchedule(bundle);
  return rv;
}

Schedule* ScheduleReader::readSchedule(const ange::stowbase::Bundle& bundle, const ange::stowbase::ReferenceObject& schedule_ro) {
  QList<Call*> calls;
  try {
    Q_FOREACH(ReferenceObject schedule_entry_ro, schedule_ro.readAllReferencedObjects("entries", bundle)) {
      ReferenceObject event_ro = schedule_entry_ro.readMandatoryReferenceObject("event", bundle);
      calls << readCall(event_ro, schedule_entry_ro);
    }
  } catch (...) {
    qDeleteAll(calls);
    throw;
  }
  return new Schedule(calls);
}

void ScheduleReader::mapToCurrentSchedule(const ange::stowbase::Bundle& bundle, const ange::stowbase::ReferenceObject& schedule_ro, const ange::schedule::Schedule& schedule)
{
  const Call* last_found = 0L;
  Q_FOREACH(ReferenceObject schedule_entry_ro, schedule_ro.readAllReferencedObjects("entries", bundle)) {
    try {
      ReferenceObject call_ro = schedule_entry_ro.readMandatoryReferenceObject("event", bundle);
      QString port_long = call_ro.readMandatoryValue<QString>("port");
      QString voyage_code = call_ro.readOptionalValue<QString>("voyage");
      QString port = port_from_urn(port_long);
      const Call* candidate = (voyage_code.isEmpty() && last_found) ? last_found : schedule.calls().front();
      for (candidate = schedule.getCallByUncodeAfterCall(port, candidate);candidate && !voyage_code.isEmpty() && candidate->voyageCode() != voyage_code; candidate = schedule.getCallByUncodeAfterCall(port, candidate)) {
      }
      if (candidate) {
        d->call_map.insert(call_ro.reference(), candidate);
      }
    } catch (...) {
      // On error continue ;) Try to parse as much as we can.
    }
  }

}

Schedule* ScheduleReader::readSchedule(const ange::stowbase::Bundle& bundle) {
  return new Schedule(readCalls(bundle));
}

QList<schedule::Call*> ScheduleReader::readCalls(const Bundle& list) {
  QList<Call*> calls;
  // Calls from JSON
  Q_FOREACH(const ReferenceObject& call, list) {
    calls << readCall(call, ReferenceObject("",""));
  }
  return calls;
}

schedule::Call* ScheduleReader::readCall(const ReferenceObject& call_ro, const ReferenceObject& entry_ro) {
  QString port_long = call_ro.readMandatoryValue<QString>("port");
  QString voyage_code = call_ro.readOptionalValue<QString>("voyage");
  double cranes = call_ro.readOptionalValue("cranes", std::numeric_limits<double>::quiet_NaN());
  double height_limit = call_ro.readOptionalValue("heightLimit", std::numeric_limits<double>::quiet_NaN());
  QString crane_rule_string = call_ro.readOptionalValue<QString>("craneRule");
  QString twinlift_string = call_ro.readOptionalValue<QString>("twinliftRule");
  double craneProductivity = call_ro.readOptionalValue("craneProductivity", 30.0);
  QDateTime eta;
  QDateTime etd;
  QString eta_string = entry_ro.readOptionalValue<QString>("begins");
  QString etd_string = entry_ro.readOptionalValue<QString>("ends");
  if (!eta_string.isNull()) {
    eta = QDateTime::fromString(eta_string, Qt::ISODate);
  }
  if (!etd_string.isNull()) {
    etd = QDateTime::fromString(etd_string, Qt::ISODate);
  }
  QString port = port_from_urn(port_long);
  QString name = call_ro.readOptionalValue("name", port);
  ange::schedule::Call* call = new Call(port,voyage_code, name, eta, etd);
  call->setCranes(cranes);
  call->setHeightLimit(height_limit);
  call->setCraneProductivity(craneProductivity);
  if (!crane_rule_string.isNull()) {
    call->setCraneRule(stringToCraneRules(crane_rule_string));
  }
  if (!twinlift_string.isNull()) {
    call->setTwinlifting(stringToTwinLiftRules(twinlift_string));
  }
  d->call_map.insert(call_ro.reference(), call);
  return call;
}

const ange::schedule::Call* ScheduleReader::call(const QString& reference) const {
  return d->call_map.value(reference);
}

ScheduleReader::~ScheduleReader()
{

}

ScheduleReader::ScheduleReader(): d(new schedule_reader_private_t)
{

}

} // stowbase
} // ange
