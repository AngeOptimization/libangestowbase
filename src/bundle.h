#ifndef LIBANGESTOWBASE_BUNDLE_H
#define LIBANGESTOWBASE_BUNDLE_H

#include <QVariant>
#include <QSharedDataPointer>
#include "angestowbase_export.h"
#include "referenceobject.h"

namespace ange {
namespace stowbase {

/**
 * Bundle represents a list of stowbase objects, see http://kenai.com/projects/stowbase/pages/RestResourcesOverview
 */
class ANGESTOWBASE_EXPORT Bundle {
  public:
    typedef QList<ReferenceObject>::iterator iterator;
    typedef QList<ReferenceObject>::const_iterator const_iterator;

    /**
     * Create empty bundle
     */
    Bundle();

    /**
     * Destructor
     */
    ~Bundle();

    /**
     * Copy constructor (declared to cure scope issues)
     */
    Bundle(const Bundle& other);

    /**
     * Assignment (declared to cure scope issues)
     */
    const Bundle& operator=(const Bundle& rhs);

    /**
    * Write all data in the bundle to
    * @param device
    */
    void write(QIODevice* device) const;

    /**
    * Write all data in the bundle uncompressed to
    * @param device
    */
    void write_uncompressed(QIODevice* device) const;

    /**
    * Write all data in the bundle to a new file named
    * @param filename
    */
    void write(QString filename) const;

    /**
     * Read all data from
     * @param device into bundle
     */
    void read(QIODevice* device);

    /**
     * @return object_reference by partial match, or empty string if not found
     * Prints warning if multiple matches are found.
     */
    ReferenceObject find(QString object_type, QVariantMap attributes = QVariantMap()) const;

    /**
     * @return object with reference
     * @param reference
     */
    ReferenceObject reference(QString reference) const;

    /**
     * @return object with reference
     * @param reference
     * Convinience overload
     */
    ReferenceObject reference(QVariant reference) const;

    /**
     * Add
     * @param object to bundle
     * If an object with the same reference is already added, throw std::runtime_error
     */
    void operator<<(const ReferenceObject& object);

    /**
     * clear
     */
    void clear();

    /**
     * @return true if empty
     */
    bool empty() const ;

    /**
     * build a new object
     * @param group name of group (~ class of object)
     * Note that you still need to add it to the bundle
     */
    ReferenceObject build(const QString& group);

    // iterator stuff
    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;
  private:
    class BundlePrivate;
    QSharedDataPointer<BundlePrivate> d;
};

} // stowbase
} // ange

#endif
