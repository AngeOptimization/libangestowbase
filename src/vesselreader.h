#ifndef LIBANGESTOWBASE_VESSEL_READER_H
#define LIBANGESTOWBASE_VESSEL_READER_H

#include "angestowbase_export.h"
#include <QSharedDataPointer>
#include <ange/vessel/decklevel.h>

class QIODevice;
namespace ange {
namespace vessel {
class Vessel;
class Stack;
class HatchCover;
class VesselTank;
class BlockWeight;
class StackSupport;
class Point3D;
class BilinearInterpolator;
}
namespace stowbase {
class Bundle;
class ReferenceObject;
}
}

namespace ange {
namespace stowbase {

class ANGESTOWBASE_EXPORT VesselReader {

public:
    /**
     * Default constructor
     */
    VesselReader();

    ~VesselReader();

    /**
     * Read and parse the vessel from the device,
     * Will throw a std::runtime_error if the parsing fails.
     */
    ange::vessel::Vessel* readVessel(QIODevice* device);

    /**
     * Parse the vessel from the QVariantList,
     * Will throw a std::runtime_error if the parsing fails.
     */
    ange::vessel::Vessel* readVessel(const ange::stowbase::Bundle& bundle);

    /**
     * @return vessel from bundle given a reference object to start with
     * Will throw a std::runtime_error if the parsing fails.
     */
    ange::vessel::Vessel* readVessel(const ange::stowbase::Bundle& bundle, const ReferenceObject& vessel_profile);

    /**
     * @return tank for reference, or null if reference does not point to a tank id
     */
    ange::vessel::VesselTank* vesselTank(const QString& reference) const;

private:
    vessel::StackSupport* readSupport40(ange::stowbase::ReferenceObject json_support_40, ange::vessel::StackSupport* support_20_fore,
                                          ange::vessel::StackSupport* support_20_aft, const QString& row, QPair<bool, bool> has_center);

    ange::vessel::Stack* add_stack(const ange::stowbase::ReferenceObject& json_stack,
                                   const ange::stowbase::Bundle& bundle, const QPair<bool, bool> has_center,
                                   qreal stackMedianLcg);
    ange::vessel::HatchCover* add_lid(const ange::stowbase::ReferenceObject& json_lid,
                                      const QHash<QString, ange::vessel::Stack*>& reference_to_stack);
    ange::vessel::VesselTank* addTank(const ange::stowbase::ReferenceObject& tank_ro, const Bundle&  bundle);
    ange::vessel::BlockWeight add_blockweight(const ange::stowbase::ReferenceObject& block_ro, bool constant_weight);
    ange::vessel::Vessel* read_vessel_implementation(const ange::stowbase::Bundle& bundle, const ange::stowbase::ReferenceObject& vessel_profile,
                                                     const QList<ange::stowbase::ReferenceObject>& json_lids,
                                                     const QList<ange::stowbase::ReferenceObject>& json_vessel_stacks);
    static QList<qreal> read_mandatory_data_string(const ange::stowbase::ReferenceObject& object, const QString& name);
    static ange::vessel::BilinearInterpolator read2dInterpolator(const ReferenceObject& object);
    /**
     * @return whether the tank is guessed to be a ballast tank or not
     */
    bool guessBallastTank(QString description, QString group, qreal density, const ange::vessel::BilinearInterpolator& fsm) const;
    ange::vessel::StackSupport* read_support_20(ange::stowbase::ReferenceObject json_support_20, ange::stowbase::ReferenceObject json_support_40,
                                                const QString& row, const QPair<bool, bool> has_center);
    double read_stack_support_height(ange::stowbase::ReferenceObject json_support, const ange::vessel::Point3D& bottom_pos);
    double read_stack_support_weight(ReferenceObject json_Support);
    ange::vessel::Point3D read_stack_support_bottom_position(ange::stowbase::ReferenceObject json_support, const QString& bay,
                                                             const QString& row, ange::vessel::DeckLevel level, const QPair< bool, bool > has_center);
    static void checkTCGspacing(const QList< ange::vessel::Stack* >& originalStacks);

private:
    class VesselReaderPrivate;
    QSharedDataPointer<VesselReaderPrivate> d;

};

} // stowbase
} // ange

#endif // LIBANGESTOWBASE_VESSEL_READER_H
