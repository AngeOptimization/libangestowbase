/*
 *
 */

#include "containerbundlingreader.h"
#include "referenceobject.h"
#include "bundle.h"
#include <QSharedData>

namespace ange {
    namespace stowbase {

class ContainerBundlingReaderPrivate : public QSharedData
{
    public:
        ContainerBundlingReaderPrivate(const Bundle& json_bundle) : m_bundle(json_bundle) {

        }
        const Bundle& m_bundle;
};

ContainerBundlingReader::ContainerBundlingReader(const Bundle& json_bundle) : d(new ContainerBundlingReaderPrivate(json_bundle)) {

}

ContainerBundlingReader::ContainerBundlingReader(const ContainerBundlingReader& other) : d(other.d) {

}

ContainerBundlingReader& ContainerBundlingReader::operator=(const ContainerBundlingReader& other) {
    if(&other != this) {
        d = other.d;
    }
    return *this;
}

ContainerBundlingReader::ContainerBundlingData ContainerBundlingReader::read(const ReferenceObject& containerBundling) {
    QString containerReference = containerBundling.readMandatoryValue<QString>("bottomContainer");
    QStringList bundledContainers = containerBundling.readMandatoryValue<QStringList>("bundledContainers");

    return ContainerBundlingData(containerReference, bundledContainers);

}

ContainerBundlingReader::ContainerBundlingData ContainerBundlingReader::read(const QString& containerBundlingReference) {
    ReferenceObject ro = d->m_bundle.reference(containerBundlingReference);
    return read(ro);
}


ContainerBundlingReader::~ContainerBundlingReader() {

}





    } // namespace stowbase
} // namespace ange
