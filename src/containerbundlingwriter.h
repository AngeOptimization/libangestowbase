/*
 *
 */

#ifndef CONTAINERBUNDLINGWRITER_H
#define CONTAINERBUNDLINGWRITER_H

#include <QSharedDataPointer>
#include <QString>

#include "angestowbase_export.h"


namespace ange {
namespace stowbase {

class Bundle;

class ContainerBundlingWriterPrivate;

class ANGESTOWBASE_EXPORT ContainerBundlingWriter {
    public:
        ContainerBundlingWriter(ange::stowbase::Bundle& bundle);
        ContainerBundlingWriter(const ContainerBundlingWriter& other);
        ~ContainerBundlingWriter();
        ContainerBundlingWriter& operator=(const ContainerBundlingWriter& other);
        /**
         * Write a container to the bundle
         */
        QString operator()(const QString& containerReference, const QStringList& bundledContainers );
    private:
        QSharedDataPointer<ContainerBundlingWriterPrivate> d;
};

}
}

#endif // CONTAINERBUNDLINGWRITER_H
