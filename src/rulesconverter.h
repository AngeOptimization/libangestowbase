#ifndef ANGE_STOWBASE_RULESCONVERTER
#define ANGE_STOWBASE_RULESCONVERTER

#include <ange/schedule/cranerules.h>
#include <ange/schedule/twinliftrules.h>

#include <QString>

namespace ange {
namespace stowbase {

QString craneRulesToString(ange::schedule::CraneRules::Types craneRules);

ange::schedule::CraneRules::Types stringToCraneRules(QString string);

QString twinLiftRulesToString(ange::schedule::TwinLiftRules::Types twinLiftRules);

ange::schedule::TwinLiftRules::Types stringToTwinLiftRules(QString string);

}}  // namespace ange::schedule

#endif  // ANGE_STOWBASE_RULESCONVERTER
