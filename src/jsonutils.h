#ifndef LIBANGESTOWBASE_JSON_UTILS_H
#define LIBANGESTOWBASE_JSON_UTILS_H

#include <QString>
#include <QVariant>
#include <stdexcept>
#include "angestowbase_export.h"
#include <cmath>
#include <ange/vessel/decklevel.h>

namespace ange {
  namespace containers {
    class IsoCode;
  }
}

namespace ange {
namespace stowbase {


/**
* Functions for translating the C++ type to a string describing the JSON type.
*/
template <typename T> struct json_type_as_string {
  static QString string() {
    return "unknown";
  }
};
template <> struct json_type_as_string<QVariantMap> {
  static QString string() {
    return "object";
  }
};
template <> struct json_type_as_string<QVariantList> {
  static QString string() {
    return "array";
  }
};
template <> struct json_type_as_string<QString> {
  static QString string() {
    return "string";
  }
};
template <> struct json_type_as_string<bool> {
  static QString string() {
    return "boolean";
  }
};
template <> struct json_type_as_string<qreal> {
  static QString string() {
    return "number";
  }
};

/**
* Cast a QVariant if possible, otherwise throw std::runtime_error.
* Workaround the extremely forgiving cast that is build into QVariant.
*/
template <typename T> T vcast(QVariant qVariant) {
  if (not qVariant.canConvert<T>()) {
    throw std::runtime_error(QString("Expected JSON %1, got '%2'")
      .arg(json_type_as_string<T>::string())
      .arg(qVariant.toString())
      .toStdString());
  }
  return qVariant.value<T>();
}

/**
* Will cast the QVariant to a QVariantMap and then read a value from the map and return it.
* @throws std::runtime_error if there is any kind if unexpected data
*/
template<typename T> T readMandatoryValue(const QVariant& map, const QString& key) {
  return vcast<T>(readMandatoryValue<T>(vcast<QVariantMap>(map), key));
}


/*
 * helper for readMandatoryValue
 */
template<typename T> T helper_for_readMandatoryValue(const QVariantMap& map, const QString& key) {
  QVariantMap::const_iterator it = map.find(key);
  if (it == map.end()) {
    QString mapvalues;
    for (QVariantMap::const_iterator i = map.begin(), iend = map.end(); i != iend; ++i) {
      mapvalues += (i.key() + "='" + i.value().toString() + "' ");
    }
    throw std::runtime_error(QString("Expected value for '%1', but no such entry. Map contained %2").arg(key).arg(mapvalues).toStdString());
  }
  return ange::stowbase::vcast<T>(it.value());
}


/**
* Will read a value from the map and return it.
* @throws std::runtime_error if there is any kind if unexpected data
*/
template<typename T> T readMandatoryValue(const QVariantMap& map, const QString& key) {
  return  helper_for_readMandatoryValue<T>(map,key);
}

// Specialization for qreal that warns about nans
template<> inline qreal readMandatoryValue(const QVariantMap& map, const QString& key) {
  const qreal rv = helper_for_readMandatoryValue<qreal>(map, key);
  if (std::isnan(rv)) {
    qWarning("Value of key '%s' was NaN. This will probably not work. Please fix input file", key.toLocal8Bit().data());
  }
  return rv;
}

/*
 * Helper for readOptionalValue
 */
template<typename T> T helper_for_readOptionalValue(const QVariantMap& map, const QString& key, const T& def) {
  QVariantMap::const_iterator it = map.find(key);
  if (it == map.end()) {
    return def;
  }
  if (not it.value().canConvert<T>()) {
    return def;
  }
  return ange::stowbase::vcast<T>(it.value());
}

/**
* Will read a value from the map and return it.
* Will not throw an exception if it is not possible, if will just return def.
*/
template<typename T> T readOptionalValue(const QVariantMap& map, const QString& key, const T& def = T()) {
  return helper_for_readOptionalValue<T>(map,key,def);
}

/**
 * Specialization for qreals, which checks for nan and return def if nan is encountered
 */
template<> inline qreal readOptionalValue(const QVariantMap& map, const QString& key, const qreal& def) {
  const qreal rv = helper_for_readOptionalValue<qreal>(map,key,def);
  return std::isnan(rv) ? def : rv;
}


/**
* Read a (NOT DONT YET: compressed or (can we do both?)) uncompressed device and parse as JSON.
* Throws std::runtime_error if the JSON is not ok.
*/
ANGESTOWBASE_EXPORT QVariant json_read(QIODevice* device);

/**
* Read a (NOT DONT YET: compressed or) uncompressed file and parse as JSON.
* Throws std::runtime_error if the JSON is not ok.
*/
ANGESTOWBASE_EXPORT QVariant json_read(const QString& file_name);

/**
* Write the data as (NOT DONT YET: compressed) JSON.
*/
ANGESTOWBASE_EXPORT void json_write(const QVariant& data, QIODevice* device);

/**
 * write the data uncompressed
 */
ANGESTOWBASE_EXPORT void json_write_uncompressed(const QVariant& data, QIODevice* device);

/**
* Write the data as (NOT DONT YET: compressed) JSON.
*/
ANGESTOWBASE_EXPORT void json_write(const QVariant& data, const QString& file_name);

/**
* Position to urn.
* "urn:stowbase.org:vessel:imo=%1"
*/
ANGESTOWBASE_EXPORT QString vessel_to_urn(QString imo);

/**
* Position to urn.
* "urn:stowbase.org:vessel:imo=%1,bay=%2,row=%3,tier=%4"
*/
ANGESTOWBASE_EXPORT QString vessel_position_to_urn(QString imo, int bay, int row, int tier);

/**
* Position to urn.
* "urn:stowbase.org:vessel:imo=%1,bay=%2,row=%3"
*/
ANGESTOWBASE_EXPORT QString vessel_stack_position_to_urn(QString imo, int bay, int row, ange::vessel::DeckLevel level);

/**
 * Urn to stack position
 * return true on success.
 */
ANGESTOWBASE_EXPORT bool urn_to_vessel_stack_position(QString urn, QString& imo, int& bay, int& row, ange::vessel::DeckLevel& level);

/**
* Port code to urn.
* "urn:stowbase.org:port:unlocode=%1"
*/
ANGESTOWBASE_EXPORT QString port_to_urn(QString uncode);

/**
* Urn to port code.
*/
ANGESTOWBASE_EXPORT QString port_from_urn(const QString urn);

/**
* Isocode to urn.
* "urn:stowbase.org:container:iso=%1"
*/
ANGESTOWBASE_EXPORT QString IsoCodeo_urn(const ange::containers::IsoCode& isocode);

/**
* Container id to urn.
* "urn:stowbase.org:container:id=%1"
*/
ANGESTOWBASE_EXPORT QString container_id_to_urn(const QString& container_id);

/**
* Set container id on object.
*/
ANGESTOWBASE_EXPORT void set_container_id( QVariantMap& object, const QString& container_id);

} // stowbase
} // ange

#endif
