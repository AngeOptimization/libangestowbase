#ifndef ANGE_STOWBASE_LASHINGPATTERNREADER_H
#define ANGE_STOWBASE_LASHINGPATTERNREADER_H

#include "angestowbase_export.h"

namespace ange {
namespace stowbase {
class Bundle;
class ReferenceObject;
}
namespace vessel {
class LashingPatternData;
}
}

namespace ange {
namespace stowbase {

/**
 * Read lashing pattern data out of the bundle
 */
ANGESTOWBASE_EXPORT const vessel::LashingPatternData* readLashingPatternData(const Bundle& bundle,
                                                                             const ReferenceObject& vesselProfile);

}} // namespace ange::stowbase

#endif // ANGE_STOWBASE_LASHINGPATTERNREADER_H
