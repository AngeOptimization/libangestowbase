#include "schedulewriter.h"

#include "rulesconverter.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

#include <QStringList>

namespace ange {
using schedule::Call;
using schedule::Schedule;
namespace stowbase {

struct schedule_writer_private_t {
  QString reference;
  QHash<const Call*, QString> m_call_references;
  QVariantList m_schedule_entry_references;
  bool m_schedule_written;
  schedule_writer_private_t() : m_schedule_written(false) {}
};

ScheduleWriter::ScheduleWriter() : d(new schedule_writer_private_t()) {
  // declared so that schedule_writer_private_t's destructor (or lack thereof) is in scope
}
ScheduleWriter::~ScheduleWriter() {
  // declared so that schedule_writer_private_t's destructor (or lack thereof) is in scope
}

void ScheduleWriter::writeCall(ange::stowbase::Bundle& bundle, const ange::schedule::Call* call) {
  ReferenceObject ro = bundle.build("call");
  ro.insertString("port", ange::stowbase::port_to_urn(call->uncode()));
  ro.insertString("name", call->name());
  if (!call->voyageCode().isEmpty()) {
    ro.insertString("voyage", call->voyageCode());
  }
  if (!std::isnan(call->cranes())) {
    ro.insertDouble("cranes", call->cranes());
  }
  if (!std::isnan(call->heightLimit())) {
    ro.insertDouble("heightLimit", call->heightLimit());
  }
  QString crane_rule_string = craneRulesToString(call->craneRule());
  ro.insertString("craneRule", crane_rule_string);
  QString twinlift_rule_string = twinLiftRulesToString(call->twinLifting());
  ro.insertString("twinliftRule",twinlift_rule_string);
  if (!std::isnan(call->craneProductivity())) {
    ro.insertDouble("craneProductivity", call->craneProductivity());
  }
  bundle << ro;
  d->m_call_references.insert(call, ro.reference());
  ReferenceObject schedule_entry(bundle.build("scheduleEntry"));
  schedule_entry.insertReference("event", ro.reference());
  if (call->eta().isValid()) {
    schedule_entry.insertString("begins", call->eta().toString(Qt::ISODate));
  }
  if (call->etd().isValid()) {
    schedule_entry.insertString("ends", call->eta().toString(Qt::ISODate));
  }
  d->m_schedule_entry_references << schedule_entry.reference();
  bundle << schedule_entry;
}

void ScheduleWriter::writeSchedule(ange::stowbase::Bundle& bundle) {
  ReferenceObject ro(bundle.build("schedule"));
  ro.insertReferences("entries", d->m_schedule_entry_references);
  d->reference = ro.reference();
  bundle << ro;
}

void ScheduleWriter::write(Bundle& bundle, const Schedule* schedule) {
  const QList<Call*>& list = schedule->calls();
  Q_FOREACH(const Call* call, list) {
    writeCall(bundle, call);
  }
  writeSchedule(bundle);
}

void ScheduleWriter::write(const Schedule* schedule,QIODevice* device) const {
  Bundle bundle;
  Q_FOREACH(const Call* call,schedule->calls()) {
    ReferenceObject ro(bundle.build("call"));
    ro.insertString("port",ange::stowbase::port_to_urn(call->uncode()));
  }
  bundle.write(device);
}


QString ScheduleWriter::reference() const {
  return d->reference;
}

QString ScheduleWriter::reference(const Call* call) const {
  return d->m_call_references.value(call);
}

} // stowbase
} // ange
