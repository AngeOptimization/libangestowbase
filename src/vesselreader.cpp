#include "vesselreader.h"

#include "bundle.h"
#include "jsonutils.h"
#include "lashingpatternreader.h"

#include <ange/containers/container.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/blockweight.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/point3d.h>
#include <ange/vessel/bayrowtier.h>
#include <ange/vessel/cargohold.h>
#include <ange/vessel/vesseltank.h>
#include <ange/vessel/bilinearinterpolator.h>
#include <ange/vessel/hatchcover.h>
#include <ange/vessel/stacksupport.h>

#include <QStringList>
#include <QMetaObject>
#include <QMetaEnum>

#include <limits>
#include <cmath>

using namespace ange::units;
using namespace ange::vessel;
using std::isinf;
using std::isnan;

namespace ange {
namespace stowbase {

struct VesselReader::VesselReaderPrivate : public QSharedData {
    VesselReaderPrivate() : has_stack_positions(false) {}
    enum issue_t {
      NO_STACK_SUPPORT_POSITION,
      BOGUS_EXCEL_NUMBER_IN_MAX_WEIGHT,
      NAN_IN_MAX_WEIGHT,
      FEU_BAY_INVALID,
      TIER_LIST_EMPTY
    };
    typedef QHash<issue_t, QStringList > issues_t;
    issues_t issues;
    bool has_stack_positions;

    void report_local_issue(issue_t issue, QString reference);
    void report_global_issue(issue_t issue);

    QHash<QString, QStringList> get_issues() const;
    static QString issue_as_string(issue_t issue);
    QHash<QString, ange::vessel::VesselTank*> tanks_deprecated;
    QHash<QString, ange::vessel::VesselTank*> m_tanks;
    QHash<QString, ange::vessel::BlockWeight> lightship_block_weights;
    QHash<QString, ange::vessel::BlockWeight> constant_block_weights;
};

QString VesselReader::VesselReaderPrivate::issue_as_string(VesselReaderPrivate::issue_t issue) {
  switch (issue) {
    case NO_STACK_SUPPORT_POSITION:
      return "No position for stack support. A position was created from bay, row and tier numbers";
    case BOGUS_EXCEL_NUMBER_IN_MAX_WEIGHT:
      return "Magic Bogus Excel number in max stack weight encountered. The stack weight will be ignored";
    case NAN_IN_MAX_WEIGHT:
      return "NaN (Not a number) in max stack weight encountered. The stack weight will be ignored";
    case FEU_BAY_INVALID:
      return "Feu bay contained an invalid value.";
    case TIER_LIST_EMPTY:
      return "Stack support has no valid tiers";
  }
  Q_ASSERT(false);
  return QString("unknown issue");
}


QHash< QString, QStringList > VesselReader::VesselReaderPrivate::get_issues() const {
  QHash< QString, QStringList > rv;
  for (issues_t::const_iterator it = issues.begin(), iend = issues.end(); it != iend; ++it) {
    rv[issue_as_string(it.key())] << it.value();
  }
  return rv;
}

void VesselReader::VesselReaderPrivate::report_global_issue(VesselReaderPrivate::issue_t issue) {
  issues[issue];
}

void VesselReader::VesselReaderPrivate::report_local_issue(VesselReaderPrivate::issue_t issue, QString reference) {
  issues[issue] << reference;
}

VesselReader::VesselReader() : d(new VesselReaderPrivate)
{
    // Empty
}

VesselReader::~VesselReader() {
    // Empty
}

Vessel* VesselReader::readVessel(QIODevice* device) {
    Bundle bundle;
    bundle.read(device);
    return readVessel(bundle);
}

Vessel* VesselReader::readVessel(const Bundle& bundle) {
    return readVessel(bundle, bundle.find("vesselProfile"));
}

Vessel* VesselReader::readVessel(const ange::stowbase::Bundle& bundle, const ange::stowbase::ReferenceObject& vessel_profile) {
    QList<ReferenceObject> json_lids = vessel_profile.readAllReferencedObjects("vesselLids", bundle);
    QList<ReferenceObject> json_stacks = vessel_profile.readAllReferencedObjects("vesselStacks", bundle);
    return read_vessel_implementation(bundle, vessel_profile, json_lids, json_stacks);
}

Stack* VesselReader::add_stack(const ange::stowbase::ReferenceObject& json_stack, const ange::stowbase::Bundle& bundle,
                               const QPair< bool, bool > baySlicesHaveCenterRow, qreal medianStackLcg) {
    const QString row = json_stack.readMandatoryValue<QString>("rowName");
    QString overlapping_feu_bay_name = json_stack.readMandatoryValue<QString>("overlappingFeuBay");
    double stack_center_to_the_fore_by = json_stack.readOptionalValue("centerToTheForeInM", std::numeric_limits< qreal >::quiet_NaN());
    ReferenceObject json_support_20_fore("","");
    ReferenceObject json_support_20_aft("","");
    ReferenceObject json_support_40("","");
    Q_FOREACH (ReferenceObject json_support, json_stack.readAllReferencedObjects("vesselStackSupports", bundle)) {
        if (json_support.readMandatoryValue<qreal>("lengthAlongshipInM") > 30 * feet/meter) {
            if (!json_support_40.empty()) {
                throw std::runtime_error(QString("stack %1,%2 with 2 40' stack supports").arg(overlapping_feu_bay_name).arg(row).toStdString());
            }
            json_support_40 = json_support;
        } else {
            bool stack_support_fore = true;
            const double centerToTheForeBy = json_support.readOptionalValue("centerToTheForeInM", std::numeric_limits< qreal >::quiet_NaN());
            if (!std::isnan(stack_center_to_the_fore_by) && !std::isnan(centerToTheForeBy)) {
                // If we have stack bottom position, use that to determine if stack support is aft or fore
                stack_support_fore = (centerToTheForeBy > medianStackLcg - 0.5);
            } else {
                // If not, use the bay numbers, falling back on assuming fore
                bool ok;
                int overlappingFeuBayNumeric = overlapping_feu_bay_name.toInt(&ok);
                const QString support_bay = json_support.readMandatoryValue<QString>("bayName");
                int support_bay_numeric = -1;
                if (ok) {
                    support_bay_numeric = support_bay.toInt(&ok);
                }
                if (ok) {
                    stack_support_fore = (support_bay_numeric < overlappingFeuBayNumeric);
                }
            }
            if (stack_support_fore) {
                if (!json_support_20_fore.empty()) {
                    // 2 supports fore...
                    const double other_centerToTheForeBy = json_support_20_fore.readOptionalValue("centerToTheForeInM", std::numeric_limits< qreal >::quiet_NaN());
                    if (std::fabs(centerToTheForeBy-other_centerToTheForeBy)<0.1) {
                        throw std::runtime_error(QString("stack %1,%2 with 2 20' stacks with identical position").arg(overlapping_feu_bay_name).arg(row).toStdString());
                    }
                    if (!json_support_20_aft.empty()) {
                        throw std::runtime_error(QString("stack %1,%2 with 3 or more  20' stacks").arg(overlapping_feu_bay_name).arg(row).toStdString());
                    }
                    if (centerToTheForeBy < other_centerToTheForeBy) {
                        json_support_20_aft = json_support_20_fore;
                        json_support_20_fore = json_support;
                    } else {
                        json_support_20_aft = json_support;
                    }
                } else {
                        json_support_20_fore = json_support;
                }
            } else {
                // aft
                if (!json_support_20_aft.empty()) {
                    const double other_centerToTheForeBy = json_support_20_aft.readOptionalValue("centerToTheForeInM", std::numeric_limits< qreal >::quiet_NaN());
                    // 2 supports aft...
                    if (std::fabs(centerToTheForeBy-other_centerToTheForeBy)<0.1) {
                        throw std::runtime_error(QString("stack %1,%2 with 2 20' stacks with identical position").arg(overlapping_feu_bay_name).arg(row).toStdString());
                    }
                    if (!json_support_20_fore.empty()) {
                        throw std::runtime_error(QString("stack %1,%2 with 3 or more  20' stacks").arg(overlapping_feu_bay_name).arg(row).toStdString());
                    }
                    if (centerToTheForeBy < other_centerToTheForeBy) {
                        json_support_20_aft = json_support;
                    } else {
                        json_support_20_aft = json_support_20_fore;
                        json_support_20_fore = json_support;
                    }
                } else {
                    json_support_20_aft = json_support;
                }
            }
        }
    }
    // Ok, now we have the necessary supports.
    QScopedPointer<StackSupport> stack_support_20_fore;
    QScopedPointer<StackSupport> stack_support_20_aft;
    QScopedPointer<StackSupport> stack_support_40;
    if (!json_support_20_fore.empty()) {
        stack_support_20_fore.reset(read_support_20(json_support_20_fore, json_support_40, row, baySlicesHaveCenterRow));
    }
    if (!json_support_20_aft.empty()) {
        stack_support_20_aft.reset(read_support_20(json_support_20_aft, json_support_40, row, baySlicesHaveCenterRow));
    }
    if (!json_support_40.empty()) {
        stack_support_40.reset(readSupport40(json_support_40, stack_support_20_fore.data(), stack_support_20_aft.data(), row, baySlicesHaveCenterRow));
    }
    if (!(stack_support_20_aft ||  stack_support_20_fore || stack_support_40)) {
        throw std::runtime_error(QString("stack %1,%2 with no stack supports").arg(overlapping_feu_bay_name).arg(row).toStdString());
    }
    if (std::isnan(stack_center_to_the_fore_by)) {
        // We don't have the stack bottom position. Instead, use the bay number *(-4m) (!), to at least get some reasonable(!) numbers
        ange::units::Length x = 0.0*meter;
        int count = 0;
        if (stack_support_20_aft) {
            x += stack_support_20_aft->bottomPos().l() + 10*ange::units::feet;
            ++count;
        }
        if (stack_support_20_fore) {
            x += stack_support_20_fore->bottomPos().l() - 10*ange::units::feet;
            ++count;
        }
        if (stack_support_40) {
            x += stack_support_40->bottomPos().l();
            ++count;
        }
        Q_ASSERT(count>0);
        stack_center_to_the_fore_by = x/meter/count; // count>0 due to check above that each stack has at least 1 support
    }
    const qreal stack_bottom_above_by = json_stack.readOptionalValue("bottomAboveInM", std::numeric_limits< double >::quiet_NaN());
    const qreal stack_center_to_the_port_by = json_stack.readOptionalValue("centerToThePortInM", std::numeric_limits< double >::quiet_NaN());
    // Point3D has positive TCG to starboard, but JSON format has positive TCG to port, see #1345
    ange::vessel::Point3D stack_bottomPos(stack_center_to_the_fore_by*meter, stack_bottom_above_by*meter, -1*stack_center_to_the_port_by*meter);
    // Some vessels have bogus stack bottom position, with .y() and .z() set to .666 and -.666.
    // The rest doesn't have any, so infer the positions from the supports.
    const bool bogus_stack_vcg = std::fabs(stack_bottomPos.v()/meter - 0.666)<1e-10;
    if (bogus_stack_vcg || std::isnan(stack_bottomPos.v()/meter)) {
        qreal y = std::numeric_limits<qreal>::infinity();
        if (stack_support_40) {
            y = qMin(stack_support_40->bottomPos().v()/meter, y);
        }
        if (stack_support_20_aft) {
            y = qMin(stack_support_20_aft->bottomPos().v()/meter, y);
        }
        if (stack_support_20_fore) {
            y = qMin(stack_support_20_fore->bottomPos().v()/meter, y);
        }
        if (std::isinf(y)) {
            throw std::runtime_error(QString("Unable to find vertical position of stack from object %1").arg(json_stack.reference()).toStdString());
        }
        stack_bottomPos = ange::vessel::Point3D(stack_bottomPos.l(), y*meter, stack_bottomPos.t());
    }
    const bool bogus_stack_tcg = std::fabs(stack_bottomPos.t()/meter + 0.666)<1e-10;
    if (std::isnan(stack_bottomPos.t()/meter) || bogus_stack_tcg)  {
        qreal z = std::numeric_limits<qreal>::quiet_NaN();
        if (stack_support_40) {
            z = stack_support_40->bottomPos().t()/meter;
        } else  if (stack_support_20_aft) {
            z = stack_support_20_aft->bottomPos().t()/meter;
        } else if (stack_support_20_fore) {
            z = stack_support_20_fore->bottomPos().t()/meter;
        }
        if (std::isnan(z)) {
            throw std::runtime_error(QString("Unable to find athwart position of stack from object %1").arg(json_stack.reference()).toStdString());
        }
        stack_bottomPos = ange::vessel::Point3D(stack_bottomPos.l(), stack_bottomPos.v(), z*meter);
    }
    DeckLevel level;
    if (stack_support_20_fore) {
        level = stack_support_20_fore->stackSupportLevel();
    } else if (stack_support_20_aft) {
        level = stack_support_20_aft->stackSupportLevel();
    } else {
        level = stack_support_40->stackSupportLevel();
    }
    Stack* rv = new Stack(stack_bottomPos,
                        level,
                        stack_support_20_fore.take(),
                        stack_support_20_aft.take(),
                        stack_support_40.take());
    bool twinlifting = json_stack.readOptionalValue("supportTwinlifting",true);
    rv->setTwinlifting(twinlifting);
    return rv;
}

double VesselReader::read_stack_support_height(ange::stowbase::ReferenceObject json_support, const ange::vessel::Point3D& bottomPos)
{
  double topAboveBy = json_support.readOptionalValue("topAboveInM",std::numeric_limits<qreal>::infinity());
  if (std::isnan(topAboveBy)) {
    // Sometimes, this is given as NaN, which makes little sense, but usually it means that there is no strack height.
    topAboveBy = std::numeric_limits< qreal >::infinity();
  }
  const double bottomAboveBy = bottomPos.v()/meter;
  return  topAboveBy - bottomAboveBy;
}

double VesselReader::read_stack_support_weight(ReferenceObject json_support)
{
  double max_weight = json_support.readOptionalValue("maxWeightInKg", std::numeric_limits<qreal>::infinity());
  if (std::fabs(max_weight - 0.666) < 1.0E-10) {
    d->report_local_issue(VesselReaderPrivate::BOGUS_EXCEL_NUMBER_IN_MAX_WEIGHT, json_support.reference());
    max_weight = std::numeric_limits<qreal>::infinity();
  }
  if (std::isnan(max_weight)) {
    d->report_local_issue(VesselReaderPrivate::NAN_IN_MAX_WEIGHT, json_support.reference());
    max_weight = std::numeric_limits<qreal>::infinity();
  }
  return max_weight;
}

ange::vessel::Point3D VesselReader::read_stack_support_bottom_position(ReferenceObject json_support, const QString& bay, const QString& row, DeckLevel level, const QPair< bool, bool > has_center)
{
  bool no_position = false;

  double bottomAboveBy = json_support.readOptionalValue<double>("bottomAboveInM", std::numeric_limits< qreal >::quiet_NaN());
  const QStringList tier_list = json_support.readMandatoryValue<QStringList>("dcTiersFromBelow");
  if (std::isnan(bottomAboveBy)) {
    // No bottom of stack is given. So guess one:
    switch(level) {
      case Above:
        // Have tier 80 on 0.0, and allow for a HC between each tier.
        bottomAboveBy = (tier_list.front().toInt() - 80)/2 * 2.8956;
        break;
      case Below:
        // So assume that stack is designed to hold one HC, the rest DC
        bottomAboveBy = -((tier_list.size()-1) * 2.5908 + 2.8956);
        break;
    }
    no_position = true;
  }
  double centerToTheForeBy = json_support.readOptionalValue("centerToTheForeInM", std::numeric_limits< qreal >::quiet_NaN());
  if (std::isnan(centerToTheForeBy)) {
    // No lcg. Use bay_number *-4m
    centerToTheForeBy = bay.toInt()*-4.0;
    no_position = true;
  }
  double centerToThePortBy = json_support.readOptionalValue("centerToThePortInM", std::numeric_limits<qreal>::quiet_NaN());
  if (std::isnan(centerToThePortBy)) {
    // No tcg present. Guestimate it from row name
    const double row_width = 2.5; // 8 feet-and-a-bit
    const int row_int = row.toInt();
    const double sign = (row_int % 2) == 0 ? +1.0 : -1.0;
    const int stacks_from_center = (row_int + 1)/2;
    double center_offset = 0.0; // If level+bay_slice have a center stack. If not, move starboard and port wings a half stackwidth towards the center.
    if ((level == Above && !has_center.first) || (level == Below && !has_center.second)) {
      center_offset -= row_width/2.5;
    }
    centerToThePortBy = (stacks_from_center*row_width + center_offset)*sign;
    no_position = true;
  }
  if (no_position) {
    d->report_local_issue(VesselReaderPrivate::NO_STACK_SUPPORT_POSITION, json_support.reference());
    if (d->has_stack_positions) {
      throw std::runtime_error("Some, but not all, stacks have positions. There is no way that I can reliable come up with the remaining positions.");
    }
  }
  return ange::vessel::Point3D(centerToTheForeBy*meter, bottomAboveBy*meter, -centerToThePortBy*meter);
}

StackSupport* VesselReader::read_support_20(ange::stowbase::ReferenceObject json_support_20, ange::stowbase::ReferenceObject json_support_40, const QString& row, const QPair< bool, bool > has_center)
{
  const Mass max_weight = read_stack_support_weight(json_support_20)*kilogram;
  const QString bay = json_support_20.readMandatoryValue<QString>("bayName");
  const QStringList tier_list_20 = json_support_20.readMandatoryValue<QStringList>("dcTiersFromBelow");
  if (tier_list_20.empty()) {
    d->report_local_issue(VesselReaderPrivate::TIER_LIST_EMPTY, json_support_20.reference());
    return 0L; // Cannot continue in this case, and in any case, the support would be useless
  }
  // FIXME: the current assumption is that we are under deck if tier is less than 50
  //This is considered a hack, and should be solved by
  //somehow getting the proper data from the outside, rather than
  //relying on such data conditions that might not be always true.
  DeckLevel level = tier_list_20.front().toInt() < 50 ? Below : Above;
  ange::vessel::Point3D bottomPos = read_stack_support_bottom_position(json_support_20, bay, row, level, has_center);
  const Length stack_height = read_stack_support_height(json_support_20, bottomPos)*meter;
  const QStringList reefer_tier_list = json_support_20.readOptionalValue<QStringList>("dcReeferTiersFromBelow");
  const QStringList fourtyfive_tier_list = json_support_40.readOptionalValue<QStringList>("dcFourtyFiveTiersFromBelow");
  const QStringList tier_list_40 = json_support_40.readOptionalValue<QStringList>("dcTiersFromBelow");
  ange::vessel::Point3D slot_offset(0.0*meter, 8.5 * feet, 0.0*meter);
  ange::vessel::Point3D Slotop_pos = bottomPos + slot_offset;
  QList<Slot*> slot_list;
  Q_FOREACH(const QString tier, tier_list_20) {
    Slot::Capabilities capabilities;
    capabilities |= Slot::TwentyCapable;
    if(reefer_tier_list.contains(tier)){
      capabilities |= Slot::ReeferCapable;
    }
    if(fourtyfive_tier_list.contains(tier)) {
      capabilities |= Slot::FourtyFiveCapable;
    }
    if(tier_list_40.contains(tier)) {
      capabilities |= Slot::FourtyCapable;
    }
    Slot* newslot = new Slot(Slotop_pos, BayRowTier(bay, row, tier));
    newslot->addCapabilities(capabilities);
    slot_list<< newslot;
    Slotop_pos = Slotop_pos + slot_offset;
  }
  StackSupport* rv = new StackSupport(stack_height, max_weight, bay, row,  level, StackSupport::Twenty, slot_list, bottomPos);
  rv->forbidImo(json_support_20.readOptionalValue("imoForbidden", false));
  return rv;
}

StackSupport* VesselReader::readSupport40(ange::stowbase::ReferenceObject json_support_40, ange::vessel::StackSupport* support_20_fore, ange::vessel::StackSupport* support_20_aft, const QString& row, QPair< bool, bool > has_center)
{
  const Mass max_weight = read_stack_support_weight(json_support_40)*kilogram;
  const QString bay = json_support_40.readMandatoryValue<QString>("bayName");
  const QStringList tier_list_40 = json_support_40.readMandatoryValue<QStringList>("dcTiersFromBelow");
  if (tier_list_40.empty()) {
    d->report_local_issue(VesselReaderPrivate::TIER_LIST_EMPTY, json_support_40.reference());
    return 0L; // Cannot continue in this case, and in any case, the support would be useless
  }
  // FIXME: the current assumption is that we are under deck if top tier is less than 50
  //This is considered a hack, and should be solved by
  //somehow getting the proper data from the outside, rather than
  //relying on such data conditions that might not be always true.
  DeckLevel level = tier_list_40.back().toInt() < 50 ? Below : Above;
  ange::vessel::Point3D bottomPos = read_stack_support_bottom_position(json_support_40, bay, row, level, has_center);
  Length stack_height = read_stack_support_height(json_support_40, bottomPos)*meter;
  QStringList reefer_tier_list = json_support_40.readOptionalValue<QStringList>("dcReeferTiersFromBelow");
  QStringList fourtyfive_tier_list = json_support_40.readOptionalValue<QStringList>("dcFourtyFiveTiersFromBelow");
  ange::vessel::Point3D slot_offset(0.0*meter, 8.5 * feet, 0.0*meter);
  ange::vessel::Point3D Slotop_pos = bottomPos + slot_offset;
  QList<Slot*> slot_list;
  Q_FOREACH(const QString tier, tier_list_40) {
    bool has_tier_as_20 = false;
    if (support_20_fore) {
      Q_FOREACH(Slot* slot,support_20_fore->stackSlots()) {
        if (slot->brt().tier() == tier.toInt()) {
          has_tier_as_20 = true;
          slot_list << slot;
          break;
        }
      }
    }
    if (support_20_aft) {
      Q_FOREACH(Slot* slot, support_20_aft->stackSlots()) {
        if (slot->brt().tier() == tier.toInt()) {
          has_tier_as_20 = true;
          slot_list << slot;
          break;
        }
      }
    }
    if (!has_tier_as_20) {
      Slot::Capabilities capabilities;
      capabilities |= Slot::FourtyCapable;
      if(reefer_tier_list.contains(tier)){
        capabilities |= Slot::ReeferCapable;
      }
      if(fourtyfive_tier_list.contains(tier)) {
        capabilities |= Slot::FourtyFiveCapable;
      }
      Slot* newslot = new Slot(Slotop_pos, BayRowTier(bay, row, tier));
      newslot->addCapabilities(capabilities);
      slot_list << newslot;
    }
    Slotop_pos = Slotop_pos + slot_offset;
  }
  // Sometimes, the 40' stack support maxheight is inf, but 20' stack supports are not. In this case, use the minimum.
  if (std::isinf(stack_height/meter)) {
    if (support_20_aft) {
      stack_height = std::min(stack_height, support_20_aft->maxHeight());
    }
    if (support_20_fore) {
      stack_height = std::min(stack_height, support_20_fore->maxHeight());
    }
  }
  StackSupport* rv = new StackSupport(stack_height, max_weight, bay, row,  level, StackSupport::Forty, slot_list, bottomPos);
  rv->forbidImo(json_support_40.readOptionalValue("imoForbidden", false));
  return rv;
}

HatchCover* VesselReader::add_lid(const ange::stowbase::ReferenceObject& json_lid, const QHash< QString, ange::vessel::Stack* >& reference_to_stack) {
  QList<Stack*> below;
  Q_FOREACH(const QVariant& reference, json_lid.readOptionalValue<QVariantList>("vesselStacksBeneathLid")) {
    Stack* stack = reference_to_stack.value(reference.toString());
    if (!stack) {
      throw std::runtime_error(QString("Unknown stack reference %1 in vesselStacksBeneathLid for a lid").arg(reference.toString()).toStdString());
    }
    below << stack;
  }
  QList<Stack*> above;
  Q_FOREACH(const QVariant& reference, json_lid.readOptionalValue<QVariantList>("vesselStacksOnTopLid")) {
    Stack* stack = reference_to_stack.value(reference.toString());
    if (!stack) {
      throw std::runtime_error(QString("Unknown stack reference %1 in vesselStacksOnTopLid for a lid").arg(reference.toString()).toStdString());
    }
    above << stack;
  }
  return new HatchCover(above, below);
}

bool VesselReader::guessBallastTank(QString description, QString group, qreal density,
                                       const BilinearInterpolator& fsm) const {
    //Seacos
    if(description.contains("WB") || description.contains("BW") || description.contains("WBT")){
        if(!description.contains("heel", Qt::CaseInsensitive) && !description.contains(QRegExp("[A-Z]+WB")) && !description.contains(QRegExp("[A-Z]+WBT"))) {
            return true; //should the heeling tanks really be considered as ballast tanks, since they should supposedly always be fixed  to half full?
        }else{
        //return false; //why should we return false here?
                      //A tank call e.g. TWBT would obviously be a ballast tank, but would not be considered as such given this line
        }
    }
    if(description.contains("ST") ||
        description.contains("DO") ||
        description.contains("FW") ||
        description.contains("OT") ||
        description.contains("MIS")||
        description.contains("MIC")||
        description.contains("M")  ||
        description.contains("LO") ||
        description.contains("HFO")||
        description.contains("FO") ||
        description.contains("SP") ||
        description.contains("DT") ||
        description.contains("LEAKO") ||
        description.contains("BILGE") ||
        description.contains("DIRTY") ||
        description.contains("FOSL") ||
        description.contains("LOSL") ||
        description.contains("COSL") ||
        description.contains("sludge") ||
        description.contains("bilge") ||
        description.contains("sump") ||
        description.contains("drain") ||
        description.contains("leakoil") ||
        description.contains("dirty oil") ||
        description.contains("OTH")){
        return false;
    }
    //Loadstar:
    if(!group.compare("Ballast", Qt::CaseInsensitive) ||
                        description.contains("water ballast") ||
                        !group.compare("Upper Ballast", Qt::CaseInsensitive) ||
                        !group.compare("WaterBallast", Qt::CaseInsensitive) ||
                        !group.compare("Ballast Water", Qt::CaseInsensitive) ||
                        !group.compare("Water Ballast, DB", Qt::CaseInsensitive) ||
                        !group.compare("Water Ballast, Side", Qt::CaseInsensitive) ||
                        !group.compare("Water Ballast, Tanks", Qt::CaseInsensitive)){
        return true;
    }
    double max_fsb = 0.0;
    for(int j = 0; j < fsm.xCoordinates().size(); ++j) {
        for(int i = 0; i < fsm.yCoordinates().size(); ++i) {
            max_fsb = std::max(fsm.valueAt(fsm.xCoordinates().at(j), fsm.yCoordinates().at(i)), max_fsb);
        }
    }
    if(max_fsb == 0.0||
                        !group.compare("Heel") ||
                        description.contains("heel") ||
                        !group.compare("Stores",Qt::CaseInsensitive) ||
                        description.contains("diesel") ||
                        !group.compare("Fresh water", Qt::CaseInsensitive) ||
                        !group.compare("Portable Water Tanks", Qt::CaseInsensitive) ||
                        !group.compare("Operational Fresh water", Qt::CaseInsensitive) ||
                        !group.compare("FW", Qt::CaseInsensitive) ||
                        !group.compare("Crew, Stores, Provision etc.", Qt::CaseInsensitive) ||
                        !group.compare("Operational SW", Qt::CaseInsensitive) ||
                        !group.compare("Operational Sea Water", Qt::CaseInsensitive) ||
                        !group.compare("Lubricating", Qt::CaseInsensitive) ||
                        !group.compare("Lubrication", Qt::CaseInsensitive) ||
                        !group.compare("Lubrication Oil", Qt::CaseInsensitive) ||
                        !group.compare("Lubrication Oil Tanks", Qt::CaseInsensitive) ||
                        !group.compare("Heavy Fuel Oil", Qt::CaseInsensitive) ||
                        !group.compare("Fuel Oil", Qt::CaseInsensitive) ||
                        !group.compare("Fuel", Qt::CaseInsensitive) ||
                        !group.compare("Fuel Oil Tanks", Qt::CaseInsensitive) ||
                        description.contains("Stores Forward", Qt::CaseInsensitive) ||
                        description.contains("Stores Fwd.", Qt::CaseInsensitive) ||
                        !group.compare("SP") ||
                        !group.compare("Miscellaneous", Qt::CaseInsensitive) ||
                        description.contains("cool") ||
                        description.contains("storage") ||
                        description.contains("distilled water") ||
                        description.contains("dist. water") ||
                        description.contains("water dump") ||
                        description.contains("drain") ||
                        description.contains("DT")
    ){
        return false;
    }
    //From spreadsheets.
    if(!group.compare("Water Ballast", Qt::CaseInsensitive)){
        return true;
    }
    if(!group.compare("WB", Qt::CaseInsensitive)){
        return true;
    }
    if(group.contains("Other", Qt::CaseInsensitive)){
        return false;
    }
    //Stab:
    if(description.contains("heel")||description.contains("sludge")){
        return false;
    }
    if(qFuzzyCompare(density,1025.0)){
        return true;
    }
    return false;
}


BlockWeight VesselReader::add_blockweight(const ange::stowbase::ReferenceObject& block_ro, bool constant_weight)
{
  double aftlcg = block_ro.readMandatoryValue<qreal>("aftLcgInM");
  double forelcg = block_ro.readMandatoryValue<qreal>("foreLcgInM");
  double aftdensity = block_ro.readMandatoryValue<qreal>("aftDensityInKgPrM");
  double foredensity = block_ro.readMandatoryValue<qreal>("foreDensityInKgPrM");
  double tcg;
  double vcg;
  if(constant_weight) {
    tcg = block_ro.readMandatoryValue<qreal>("tcgInM");
    vcg = block_ro.readMandatoryValue<qreal>("vcgInM");
  } else {
    tcg = block_ro.readOptionalValue<qreal>("tcgInM",std::numeric_limits< double >::quiet_NaN());
    vcg = block_ro.readOptionalValue<qreal>("vcgInM",std::numeric_limits< double >::quiet_NaN());
  }
  QString description = block_ro.readOptionalValue<QString>("description",QString());
  BlockWeight rv = BlockWeight(aftlcg*meter, aftdensity*kilogram_per_meter, forelcg*meter, foredensity*kilogram_per_meter, vcg*meter, tcg*meter);
  if(!description.isEmpty()) {
    rv.setDescription(description);
  }
  return rv;
}

ange::vessel::VesselTank* VesselReader::addTank(const ReferenceObject& tank_ro, const Bundle& bundle) {
    QString description = tank_ro.readMandatoryValue<QString>("description");
    qreal density = tank_ro.readMandatoryValue<qreal>("densityInKgprM3");
    qreal volCapacity = tank_ro.readOptionalValue<qreal>("capacityInM3");
    qreal foreEnd = tank_ro.readOptionalValue<qreal>("foreEndInM");
    qreal aftEnd = tank_ro.readOptionalValue<qreal>("aftEndInM");
    QString tankgroup = tank_ro.readOptionalValue<QString>("group");
    bool haveVC = volCapacity==0.0?false:true;
    qreal massCapacity = tank_ro.readOptionalValue<qreal>("capacityInKg");
    bool haveMC = massCapacity==0.0?false:true;
    if(!haveMC && !haveVC) {
        throw std::runtime_error(QString("Expected capacity for Tank: %1, but no such entry.").arg(description).toStdString());
    } else if ( haveMC && !haveVC ){
        volCapacity = massCapacity/density;
    } else if ( haveVC && !haveMC) {
        massCapacity = volCapacity * density;
    }
    QList<ReferenceObject> lcg_distribution = tank_ro.readAllReferencedObjects("lcgFunction",bundle);
    QList<ReferenceObject> vcg_distribution = tank_ro.readAllReferencedObjects("vcgFunction",bundle);
    QList<ReferenceObject> tcg_distribution = tank_ro.readAllReferencedObjects("tcgFunction",bundle);
    QList<ReferenceObject> fsm_distribution = tank_ro.readAllReferencedObjects("fsmFunction",bundle);
    BilinearInterpolator lcg;
    BilinearInterpolator vcg;
    BilinearInterpolator tcg;
    BilinearInterpolator fsm;
    if (!lcg_distribution.isEmpty()) {
        lcg = read2dInterpolator(lcg_distribution.front());
    }
    if (!vcg_distribution.isEmpty()) {
        vcg = read2dInterpolator(vcg_distribution.front());
    }
    if (!tcg_distribution.isEmpty()) {
        tcg = read2dInterpolator(tcg_distribution.front());
    }
    if (!fsm_distribution.isEmpty()) {
        fsm = read2dInterpolator(fsm_distribution.front());
    }
    bool is_ballast_tank = guessBallastTank(description, tankgroup, density, fsm);
    VesselTank* rv = new VesselTank(description, tankgroup, massCapacity, volCapacity, density, foreEnd, aftEnd, lcg, vcg, tcg, fsm, is_ballast_tank);
    d->m_tanks.insert(tank_ro.reference(), rv);
    return rv;
}

    //FIXME: refactor this whole function.
    //either we have stability data or we don't. There is no good reason
    //(use case) to handle all kinds of partial data availability
Vessel* VesselReader::read_vessel_implementation(const ange::stowbase::Bundle& bundle, const ange::stowbase::ReferenceObject& vessel_profile, const QList< ReferenceObject >& json_lids, const QList< ReferenceObject >& json_vessel_stacks)
{
  // Handle the vessel profile information
  QString vessel_name("unnamed");
  QString imo_number("0000000");
  QString callSign;
  QString vessel_code;
  QList<ReferenceObject> json_tanks;
  QList<ReferenceObject> json_hull_weight_blocks;
  QList<ReferenceObject> json_constant_weight_blocks;
  QList<ReferenceObject> json_trim;
  QList<ReferenceObject> json_drafts;
  QList<ReferenceObject> json_stability;
  QList<ReferenceObject> bonjean_curves;
  QList<ReferenceObject> metacentre_function;
  QList<ReferenceObject> hullweight_distribution;
  QList<ReferenceObject> max_bending_limits;
  QList<ReferenceObject> min_bending_limits;
  QList<ReferenceObject> max_shearforce_limits;
  QList<ReferenceObject> min_shearforce_limits;
  QList<ReferenceObject> max_torsion_limits;
  QList<ReferenceObject> min_torsion_limits;
  QList<ReferenceObject> holds;
  if (!vessel_profile.empty()) {
    vessel_name = vessel_profile.readOptionalValue<QString>("name", vessel_name);
    imo_number = vessel_profile.readOptionalValue("imoCode", QString());
    callSign = vessel_profile.readOptionalValue("callSign", QString());
    vessel_code = vessel_profile.readOptionalValue("vesselCode", QString());
    imo_number.remove("urn:stowbase.org:vessel:imo=");
    json_tanks = vessel_profile.readAllReferencedObjects("tanks", bundle);
    json_hull_weight_blocks = vessel_profile.readAllReferencedObjects("hullweightBlocks",bundle);
    json_constant_weight_blocks = vessel_profile.readAllReferencedObjects("constantWeightBlocks",bundle);
    json_drafts = vessel_profile.readAllReferencedObjects("draftFunction",bundle);
    json_trim = vessel_profile.readAllReferencedObjects("trimFunction",bundle);
    bonjean_curves = vessel_profile.readAllReferencedObjects("bonjeanCurve", bundle);
    metacentre_function = vessel_profile.readAllReferencedObjects("metacentreCurve",bundle);
    hullweight_distribution = vessel_profile.readAllReferencedObjects("hullweightFunction", bundle);
    max_bending_limits = vessel_profile.readAllReferencedObjects("bending_maxFunction", bundle);
    min_bending_limits = vessel_profile.readAllReferencedObjects("bending_minFunction", bundle);
    max_shearforce_limits = vessel_profile.readAllReferencedObjects("shear_maxFunction", bundle);
    min_shearforce_limits = vessel_profile.readAllReferencedObjects("shear_minFunction", bundle);
    max_torsion_limits = vessel_profile.readAllReferencedObjects("torsion_maxFunction", bundle);
    min_torsion_limits = vessel_profile.readAllReferencedObjects("torsion_minFunction", bundle);
    holds = vessel_profile.readAllReferencedObjects("holds", bundle);
    Q_ASSERT(json_trim.size()<=1); // we don't support more than one set of trim/draft data
    Q_ASSERT(json_drafts.size()<=1); // we don't support more than one set of trim/draft data
    json_stability = vessel_profile.readAllReferencedObjects("stability",bundle);
    Q_ASSERT(json_stability.size()<=1);
  }
  // Sort out stacks in overlapping bays to build bayslices
  typedef QMultiHash<QString, ReferenceObject> json_overlapping_bay_to_stack_t;
  json_overlapping_bay_to_stack_t json_overlapping_bay_to_stack;
  Q_FOREACH(ReferenceObject json_vessel_stack, json_vessel_stacks) {
    QString overlapping_feu_bay = json_vessel_stack.readMandatoryValue<QString>("overlappingFeuBay");
    json_overlapping_bay_to_stack.insertMulti(overlapping_feu_bay, json_vessel_stack);
  }
  // Build baySlices
  QList<BaySlice*> bay_slices;
  QHash<QString, Stack*> reference_to_stack;
  // First, build a map for whether slices have a center row or not. This is needed when guestimating TCG's when not present.
  QHash<QString, QPair<bool,bool> > slicesWithCenterRow;
  for (json_overlapping_bay_to_stack_t::const_iterator it = json_overlapping_bay_to_stack.begin(), iend= json_overlapping_bay_to_stack.end(); it != iend; ++it) {
    bool ok;
    const int rowName = it->readMandatoryValue<QString>("rowName").toInt(&ok);
    if (ok && rowName == 0) {
      // Center stack. Find out if above or below
      const QVariantList vesselStackSupports = it->readMandatoryValue<QVariantList>("vesselStackSupports");
      const ReferenceObject a_stack_support = bundle.reference(vesselStackSupports.first());
      const DeckLevel level = a_stack_support.readMandatoryValue<QStringList>("dcTiersFromBelow").front().toInt() > 50 ? Above : Below;
      const QString bayName = it->readMandatoryValue<QString>("overlappingFeuBay");
      switch (level) {
        case Above:
          slicesWithCenterRow[bayName].first = true;
          break;
        case Below:
          slicesWithCenterRow[bayName].second = true;
          break;
      }
    }
  }
  // Then create the actual stacks
  Q_FOREACH(const QString overlappingFeuBay, json_overlapping_bay_to_stack.uniqueKeys()) {
    QList<Stack*> stacks;
    const QList<ReferenceObject> json_stacks = json_overlapping_bay_to_stack.values(overlappingFeuBay);
    QList<qreal> stackLcgs;
    Q_FOREACH(ReferenceObject json_stack, json_stacks) {
        stackLcgs << json_stack.readOptionalValue("centerToTheForeInM", std::numeric_limits< qreal >::quiet_NaN());
    }
    qSort(stackLcgs);
    const qreal bayStackStandardLcg = stackLcgs.at(stackLcgs.size()/2);
    Q_FOREACH(ReferenceObject json_stack, json_stacks) {
      try {
        Stack* stack = add_stack(json_stack, bundle,
                                 slicesWithCenterRow[json_stack.readMandatoryValue<QString>("overlappingFeuBay")],
                                 bayStackStandardLcg);
        reference_to_stack.insert(json_stack.reference(), stack);
        stacks << stack;
      } catch (std::exception& e) {
        qWarning("Failed to add stack with reference %s to bayslice %s due to %s", json_stack.reference().toLocal8Bit().data(), overlappingFeuBay.toLocal8Bit().data(), e.what());
      }
    }
    checkTCGspacing(stacks);
    bay_slices << new BaySlice(stacks);
  }
  // Build hull weight blocks
  QList<BlockWeight> lightship_blocks;
  Q_FOREACH(const ReferenceObject& json_block, json_hull_weight_blocks) {
    try {
      BlockWeight block = add_blockweight(json_block,false);
      d->lightship_block_weights.insert(json_block.reference(),block);
      lightship_blocks << block;
    } catch (std::exception& e) {
      // We just have to make sure that no tanks are being written to the profile
    }
  }
  // Build constant weights
  QList<BlockWeight> constant_weights;
  Q_FOREACH(const ReferenceObject& json_block, json_constant_weight_blocks) {
    try {
      BlockWeight block = add_blockweight(json_block,true);
      d->constant_block_weights.insert(json_block.reference(),block);
      constant_weights << block;
    } catch (std::exception& e) {
      // We just have to make sure that no tanks are being written to the profile
    }
  }
  // Build lids
  QList<HatchCover*> lids;
  Q_FOREACH(const ReferenceObject& json_lid, json_lids) {
    HatchCover* lid = add_lid(json_lid, reference_to_stack);
    lids << lid;
  }
  // Build tanks
  QList<VesselTank*> tanks;
  Q_FOREACH(const ReferenceObject& ro, json_tanks) {
    try {
    VesselTank* tank = addTank(ro, bundle);
    tanks << tank;
    } catch (std::exception& e) {
      // We just have to make sure that no tanks are being written to the profile
    }
  }
  Vessel* vessel = new Vessel(bay_slices, lids, tanks, vessel_name, imo_number, callSign, vessel_code);
  if(!constant_weights.isEmpty()) {
    vessel->setConstantWeights(constant_weights);
  }
  if(json_trim.size() > 0) {
    try {
      BilinearInterpolator trim = read2dInterpolator(json_trim.front());
      vessel->setTrimData(trim);
    } catch(std::exception& e) {
      throw std::runtime_error(std::string("Failed to read trim data: ")+e.what());
    }
  }
  if(json_drafts.size() > 0) {
    try {
      BilinearInterpolator draft = read2dInterpolator(json_drafts.front());
      vessel->setDraftData(draft);
    } catch(std::exception& e) {
      throw std::runtime_error(std::string("Failed to read draft data: ")+e.what());
    }
  }
    if(json_stability.size()>0) {
        ReferenceObject stabobj = json_stability.front();
        const QString classificationSociety = stabobj.readOptionalValue<QString>("classificationSociety", QString());
        const qreal hull_lcg = stabobj.readOptionalValue("hullLcgInM", 0.0);
        const Length hull_vcg = stabobj.readOptionalValue<qreal>("hullVcgInM")*meter;
        const Mass hull_weight = stabobj.readOptionalValue("hullWeightInKg", 0.0)*kilogram;
        const ange::units::Length vessel_lpp = stabobj.readOptionalValue<qreal>("vesselLppInM")*meter;
        const qreal observer_lcg = stabobj.readOptionalValue<qreal>("observerLcgInM");
        const qreal observer_vcg = stabobj.readOptionalValue<qreal>("observerVcgInM");
        if(!std::isnan(observer_lcg) && !std::isnan(observer_vcg)) {
            vessel->setObserver(observer_lcg*meter, observer_vcg*meter);
        }
        vessel->setClassificationSociety(classificationSociety);
        vessel->setHullLcg(hull_lcg*meter);
        vessel->setLightshipWeight(hull_weight);
        vessel->setLpp(vessel_lpp);
        vessel->setHullVcg(hull_vcg);
        if (!metacentre_function.isEmpty()) {
            try {
                BilinearInterpolator metacenter = read2dInterpolator(metacentre_function.front());
                vessel->setMetacenterData(metacenter);
            } catch(std::exception& e) {
                throw std::runtime_error(std::string("Failed to read metacentre data: ")+e.what());
            }
        }
        if(      !max_bending_limits.isEmpty() && !min_bending_limits.isEmpty()
              && !max_shearforce_limits.isEmpty() && !min_shearforce_limits.isEmpty()
              && !max_torsion_limits.isEmpty() && !min_torsion_limits.isEmpty()
              && !lightship_blocks.isEmpty() && !bonjean_curves.isEmpty()) {
            try {
                BilinearInterpolator maxBM = read2dInterpolator(max_bending_limits.front());
                BilinearInterpolator minBM = read2dInterpolator(min_bending_limits.front());
                BilinearInterpolator maxSF = read2dInterpolator(max_shearforce_limits.front());
                BilinearInterpolator minSF = read2dInterpolator(min_shearforce_limits.front());
                BilinearInterpolator maxTM = read2dInterpolator(max_torsion_limits.front());
                BilinearInterpolator minTM = read2dInterpolator(min_torsion_limits.front());
                vessel->setStressLimits(maxBM, minBM, maxSF, minSF, maxTM, minTM);
                //now that vessel vcg is available, update the lightship blocks
                //we don't want NaNs in the vcg and tcg without a good reason.
                for(QList<BlockWeight>::iterator it = lightship_blocks.begin(); it != lightship_blocks.end(); ++it){
                    it->setTcg(0.0*meter);
                    it->setVcg(hull_vcg);
                }
                vessel->setLightshipWeights(lightship_blocks);
                BilinearInterpolator bonjean = read2dInterpolator(bonjean_curves.front());
                vessel->setBonjeanData(bonjean);
            } catch(std::exception& e) {
                throw std::runtime_error(std::string("Failed to read hull or stress data: ")+e.what());
            }
        }
    }
  Q_FOREACH(const ReferenceObject& ro, holds) {
    try {
      QScopedPointer<ange::vessel::CargoHold> hold(new ange::vessel::CargoHold());
      Q_FOREACH(QString imo_class, ro.readOptionalValue<QStringList>("dgAbovePermitted")) {
          hold->addAcceptedImoClass(imo_class, Above);
      }
      Q_FOREACH(QString imo_class, ro.readOptionalValue<QStringList>("acceptsImo")) {
          hold->addAcceptedImoClass(imo_class, Below);
      }
      Q_FOREACH(QString imo_class, ro.readOptionalValue<QStringList>("dgAboveNotPermitted")) {
          hold->addForbiddenImoClass(imo_class, Above);
      }
      Q_FOREACH(QString imo_class, ro.readOptionalValue<QStringList>("dgBelowNotPermitted")) {
          hold->addForbiddenImoClass(imo_class, Below);
      }
      Q_FOREACH(QString bayString, ro.readMandatoryValue<QStringList>("feubays")) {
        bool ok;
        const int bay = bayString.toInt(&ok);
        if (ok) {
          if (BaySlice* bay_slice = vessel->baySliceContaining(bay)) {
            bay_slice->setCargoHold(hold.data());
          } else {
            ok = false;
          }
        }
        if (!ok) {
          QVariantMap data;
          data.insert("actual", bayString);
          d->report_local_issue(VesselReaderPrivate::FEU_BAY_INVALID, ro.reference());
        }
      }
      if (!hold->baySlices().isEmpty()) {
        vessel->addCargoHold(hold.take());
      }
    } catch(std::exception& e) {
      throw std::runtime_error(std::string("Failed to read hold/bulkhead data: ")+e.what());
    }
  }
  vessel->setLashingPatternData(readLashingPatternData(bundle, vessel_profile));
  //would have preferred this to be private functionality used in vessel_t's constructor:
  if(!vessel->isOrientedAftFwd()){
      vessel->mirrorLongitudinally();
  }
  if (vessel->hasInfo(Vessel::VisibilityLineData)) {
    vessel->setAccomodation(vessel->observerLcg());
  }
  if (!d->issues.isEmpty()) {
    typedef QHash<QString, QStringList> is_t;
    is_t is = d->get_issues();
    for (is_t::iterator it = is.begin(), iend = is.end(); it != iend; ++it) {
      QString where = it.value().join(",");
      qWarning("Warning in vessel parsing: %s found in %s", it.key().toLocal8Bit().data(), where.toLocal8Bit().data());
    }
  }
#ifndef QT_NO_DEBUG
    Q_FOREACH(BaySlice* bs, bay_slices) {
        Q_ASSERT(bs->parent() == vessel);
    }
    Q_FOREACH(BaySlice* bs, bay_slices) {
        Q_ASSERT(bs->parent() == vessel);
        Q_FOREACH (const Stack* stack, bs->stacks()) {
            Q_FOREACH (const Slot* slot, stack->stackSlots()) {
                //ensure isAft() doesn't Q_ASSERT
                slot->isAft();
            }
        }
    }
#endif
  return vessel;
}

/* static */
BilinearInterpolator VesselReader::read2dInterpolator(const ReferenceObject& object) {
    QList<qreal> data = read_mandatory_data_string(object, "sampleData");
    QList<qreal> points1 = read_mandatory_data_string(object, "samplePoints1");
    QList<qreal> points2 = read_mandatory_data_string(object,"samplePoints2");
    if (data.size() != points1.size()*points2.size()) {
        throw std::runtime_error(QString("2dfunction '%1' inconsistent sized: %2 != %3*%4").arg(object.group()).arg(data.size()).arg(points1.size()).arg(points2.size()).toStdString());
    }
    return BilinearInterpolator(points1, points2, data);
}

/* static */
QList< qreal > VesselReader::read_mandatory_data_string(const ReferenceObject& object, const QString& name) {
  QList<qreal> rv;
  QString datastring = object.readMandatoryValue<QString>(name);
  Q_FOREACH(const QString& element, datastring.split(";")) {
    bool ok = false;
    float v = element.toFloat(&ok);
    if (!ok) {
      throw std::runtime_error("Value in 2dfunction '"+element.toStdString()+"' not a float '");
    }
    rv << v;
  }
  return rv;
}

ange::vessel::VesselTank* VesselReader::vesselTank(const QString& reference) const {
    return d->m_tanks.value(reference);
}

/**
 * will cause the stacks to be sorted from ABOVE -> BELOW, and from port -> starboard (assuming starboard is odd)
 */
struct StackSorter {
    bool operator()(const Stack* stack1, const Stack* stack2) const {
        if (stack1->level() == vessel::Above && stack2->level() == vessel::Below) {
            return true;
        }
        if (stack1->level() == vessel::Below && stack2->level() == vessel::Above) {
            return false;
        }
        if(stack1->row() % 2 != stack2->row() % 2) {
            return stack1->row() % 2 < stack2->row() % 2;
        }
        if(stack1->row() % 2) {
            return stack1->row() < stack2->row();
        } else {
            return stack1->row() > stack2->row();
        }
    }
};

void VesselReader::checkTCGspacing(const QList< Stack* >& originalStacks) {
    if(!originalStacks.isEmpty()) {
        QList< Stack* > stacks = originalStacks;
        qSort(stacks.begin(), stacks.end(), StackSorter());
        const Stack* stackPrevious = stacks.first();
        for(int i = 1; i < stacks.size(); ++i) {
            if((abs(stackPrevious->bottom().t() - stacks.at(i)->bottom().t())
                                            < ange::containers::Container::standardContainerWidth())
                                            && (stackPrevious->level() == stacks.at(i)->level())) {
                qWarning(QString("Probable error in stack TCGs, bay %1, row %2, %3")
                                .arg(stackPrevious->bays().first()).arg(stackPrevious->row())
                                .arg(stackPrevious->level()==vessel::Above? "ABOVE" : "BELOW").toLocal8Bit());
            }
            stackPrevious = stacks.at(i);
        }
    }
}

} // stowbase
} // ange
