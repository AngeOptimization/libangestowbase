/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010
 Copyright: See COPYING file that comes with this distribution
*/

#ifndef CONTAINER_MOVE_READER_H
#define CONTAINER_MOVE_READER_H
#include <QList>
#include <QHash>
#include <QDebug>

#include "bundle.h"
#include "angestowbase_export.h"

class container_move_t;
namespace ange {
namespace vessel {

class Vessel;
class Slot;
}

namespace schedule {

class Schedule;
class Call;
}

namespace containers {

class Container;
}

namespace stowbase {

class ScheduleReader;

class ReferenceObject;
class container_move_reader_private_t;
class ANGESTOWBASE_EXPORT ContainerMoveReader {
  public:
    struct move_private_t;
    struct ANGESTOWBASE_EXPORT Move {
      QStringList cargo;
      const ange::schedule::Call* call;
      const ange::vessel::Slot* slot;
      QString uncode;
      bool isPseudo;
      move_private_t* d;
      const ReferenceObject& rawObject() const;
      Move(const Move& move);
      ~Move();
      const Move& operator=(const Move& rhs);
      private:
      Move(QStringList cargo,
             const QString& uncode,
             const ange::schedule::Call* call,
             const ange::vessel::Slot* slot,
             bool is_pseudo,
             const ReferenceObject& raw_object);
      friend class ContainerMoveReader;
    };

    ContainerMoveReader(const ange::stowbase::Bundle bundle,
                            const ange::vessel::Vessel* vessel,
                            const ScheduleReader* schedule_reader);

    ~ContainerMoveReader();

    ContainerMoveReader(const ContainerMoveReader& orig);

    const ContainerMoveReader& operator=(const ContainerMoveReader& rhs);

    /**
     * Read a move from bundle
     */
    Move read(const ange::stowbase::ReferenceObject& move_ro);

  private:
    QString get_uncode(const QString& from_urn, const QString& to_urn) const;
    QSharedDataPointer<container_move_reader_private_t> d;
};

QDebug ANGESTOWBASE_EXPORT operator<<(QDebug dbg, const ContainerMoveReader::Move&);

}
}

#endif // CONTAINER_MOVE_READER_H
