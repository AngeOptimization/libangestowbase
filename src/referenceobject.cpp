
#include "referenceobject.h"
#include <stdexcept>
#include "bundle.h"

namespace ange {
namespace stowbase {

class reference_object_private_t : public QSharedData {
  public:
    reference_object_private_t(QString reference);
    QVariantMap object;
    QString reference;
};

reference_object_private_t::reference_object_private_t(QString reference): QSharedData(), reference(reference)
{

}


const QString& ReferenceObject::reference() const {
  return d->reference;
}

QVariantMap& ReferenceObject::object() {
  return d->object;
}

QVariantMap ReferenceObject::object() const {
  return d->object;
}

ReferenceObject::ReferenceObject(QString group, QString reference) : d(new reference_object_private_t(reference)) {
  d->object.insert("_group", group);
}

ReferenceObject::ReferenceObject(QVariantMap map) : d(new reference_object_private_t("")) {
  if (!map.contains("object")) {
    throw std::invalid_argument("map must contain a 'object' entry");
  }
  QVariant object = map.value("object");
  if (object.type() != QVariant::Map) {
    throw std::invalid_argument("'object' entry must be a map");
  }
  d->object = object.toMap();
  if (!d->object.contains("_group")) {
    throw std::invalid_argument("map must contain a _group");
  }
  d->reference = map.value("reference").toString();
  if (d->reference.isEmpty()) {
    throw std::invalid_argument("map must contain a nonempty reference");
  }
}

QString ReferenceObject::group() const {
  return d->object.value("_group").toString();
}

QVariantMap ReferenceObject::toMap() const {
  QVariantMap ret;
  ret.insert("reference", d->reference);
  ret.insert("object", d->object);
  return ret;
}

ReferenceObject::~ReferenceObject()
{

}

const ange::stowbase::ReferenceObject& ReferenceObject::operator=(const ange::stowbase::ReferenceObject & rhs) {
  if (this != &rhs) {
    d = rhs.d;
  }
  return *this;
}

ReferenceObject::ReferenceObject(const ange::stowbase::ReferenceObject& other) : d(other.d) {
  // Declared so that QSharedPointer have access to reference_object_private_t's destructor
}

bool ReferenceObject::empty() const {
  return d->object.size()<=1;
}

QVariant ReferenceObject::helperForReadMandatoryValue(const QString& key) const {
  QVariantMap::const_iterator it = d->object.find(key);
  if (it == d->object.end()) {
    QString mapvalues;
    for (QVariantMap::const_iterator i = d->object.begin(), iend = d->object.end(); i != iend; ++i) {
      mapvalues += (i.key() + "='" + i.value().toString() + "' ");
    }
    throw std::runtime_error(QString("Expected value for '%1', but no such entry. Object reference was %3, Map contained %2").arg(key).arg(mapvalues).arg(reference()).toStdString());
  }
  return it.value();
}

template<>
qreal ReferenceObject::readMandatoryValue<qreal>(const QString& key) const {
  const qreal rv = vcast<qreal>(helperForReadMandatoryValue(key));
  if (std::isnan(rv)) {
    qWarning("Value of key '%s' in object '%s' was NaN. This will probably not work. Please fix input file", key.toLocal8Bit().data(), reference().toLocal8Bit().data());
  }
  return rv;
}

template<>
QStringList ReferenceObject::readMandatoryValue< QStringList >(const QString& key) const {
  QVariantMap map = readOptionalValue<QVariantMap>(key);
  if (map.value("type").toString() == "stringlist") {
    try {
      QVariantList values = ange::stowbase::readMandatoryValue<QVariantList>(map, "stringlist");
      QStringList rv;
      Q_FOREACH(const QVariant& e, values) {
        rv << e.toString();
      }
      return rv;
    } catch(std::exception& e) {
      qDebug() << "error:" << e.what();
    }
  }
  throw std::runtime_error(QString("%1 in %2 was not a String List").arg(key).arg(reference()).toStdString());

}

template<>
QStringList ReferenceObject::readOptionalValue< QStringList >(const QString& key, const QStringList& def) const {
  QVariantMap map = readOptionalValue<QVariantMap>(key);
  if (map.empty()) {
    return def;
  }
  if (map.value("type").toString() == "stringlist") {
    try {
      QVariantList values = ange::stowbase::readMandatoryValue<QVariantList>(map, "stringlist");
      QStringList rv;
      Q_FOREACH(const QVariant& e, values) {
        rv << e.toString();
      }
      return rv;
    } catch(std::exception& e) {
      qDebug() << "error:" << e.what();
    }
  }
  throw std::runtime_error(QString("%1 in %2 was not a String List").arg(key).arg(reference()).toStdString());
}

template<>
  QStringList ReferenceObject::readOptionalValue< QStringList >(const QString& key) const {
  return readOptionalValue(key,QStringList());
}

QVariant ReferenceObject::helperForReadOptionalValue(const QString& key) const {
  return d->object.value(key);
}

void ReferenceObject::insertBool(QString key, bool value) {
  d->object.insert(key, value);

}
void ReferenceObject::insertDouble(QString key, qreal number) {
  d->object.insert(key, number);
}

void ReferenceObject::insertReference(QString key, QVariant ref) {
  QVariantList list;
  list << ref;
  insertReferences(key, list);
}

void ReferenceObject::insertReferences(QString key, QVariantList refs) {
  d->object.insert(key, refs);
}

void ReferenceObject::insertString(QString key, QString string) {
  d->object.insert(key, string);
}

void ReferenceObject::insertStringList(QString key, QStringList list) {
  QVariantList vlist;
  Q_FOREACH(const QString elem,  list) {
    vlist << elem;
  }
  insertStringList(key, vlist);
}

void ReferenceObject::insertStringList(QString name, QVariantList stringlist) {
  QVariantMap map;
  map.insert("type", "stringlist");
  map.insert("stringlist", stringlist);
  d->object.insert(name, map);

}

QList< ReferenceObject > ReferenceObject::readAllReferencedObjects(QString key, const ange::stowbase::Bundle& bundle) const {
  QList<ReferenceObject > list;
  Q_FOREACH(QVariant ref, readOptionalValue<QVariantList>(key)) {
    ReferenceObject ro = bundle.reference(ref);
    if (ro.group().isEmpty()) {
      qWarning("%s reference %s, but the latter was not found", reference().toLocal8Bit().data(), ref.toString().toLocal8Bit().data());
      continue;
    }
    list << ro;
  }
  return list;
}

ReferenceObject ReferenceObject::readMandatoryReferenceObject(QString key, const ange::stowbase::Bundle& bundle) const {
  // Could probably be done more effectively...
  QList<ReferenceObject > list = readAllReferencedObjects(key, bundle);
  if (list.size() != 1) {
    throw std::runtime_error(QString("From object %1 key %2: Expected 1 (valid) reference, got %3").arg(reference()).arg(key).arg(list.size()).toStdString());
  }
  return list.front();
}

ReferenceObject ReferenceObject::readOptionalReferenceObject(QString key, const ange::stowbase::Bundle& bundle) const {
  // Could probably be done more effectively...
  QList<ReferenceObject > list = readAllReferencedObjects(key, bundle);
  if (list.size() > 1) {
    throw std::runtime_error(QString("From object %1 key %2: Expected 0 or 1 (valid) reference, got %3").arg(reference()).arg(key).arg(list.size()).toStdString());
  }
  return list.empty() ? ReferenceObject("","") : list.front();

}

QDebug operator<<(QDebug dbg, const ange::stowbase::ReferenceObject& o) {
  return dbg << "{" << o.reference() << ":" << o.object() << "}";
}

QString ReferenceObject::readOptionalReference(QString key) const {
  QVariantList list = readOptionalValue<QVariantList>(key);
  if (list.empty()) {
    return QString();
  }
  return list.front().toString();
}


} // stowbase
} // ange
