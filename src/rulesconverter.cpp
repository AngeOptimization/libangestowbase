#include "rulesconverter.h"

using ange::schedule::CraneRules;
using ange::schedule::TwinLiftRules;

namespace ange {
namespace stowbase {

QString craneRulesToString(CraneRules::Types craneRules) {
    switch (craneRules) {
        case CraneRules::CraneRuleFourty:
            return QString("CRANE_RULE_FOURTY");
        case CraneRules::CraneRuleEighty:
            return QString("CRANE_RULE_EIGHTY");
        case CraneRules::CraneRuleFourtySuperBins:
            return QString("CRANE_RULE_FOURTY_SUPER_BINS");
        case CraneRules::CraneRuleUnknown:
            return QString("CRANE_RULE_UNKNOWN");
    }
    Q_ASSERT(false);
    return QString("");
}

CraneRules::Types stringToCraneRules(QString string) {
    QString upper(string.toUpper());
    if (upper == "CRANE_RULE_FOURTY") {
        return CraneRules::CraneRuleFourty;
    } else if (upper == "CRANE_RULE_EIGHTY") {
        return CraneRules::CraneRuleEighty;
    } else if (upper == "CRANE_RULE_FOURTY_SUPER_BINS") {
        return CraneRules::CraneRuleFourtySuperBins;
    } else {
        return CraneRules::CraneRuleUnknown;
    }
}

QString twinLiftRulesToString(TwinLiftRules::Types twinLiftRules) {
    switch (twinLiftRules) {
        case TwinLiftRules::TwinliftRuleAll:
            return QString("TWINLIFT_RULE_ALL");
        case TwinLiftRules::TwinliftRuleNothing:
            return QString("TWINLIFT_RULE_NOTHING");
        case TwinLiftRules::TwinliftRuleBelow:
            return QString("TWINLIFT_RULE_BELOW");
        case TwinLiftRules::TwinliftRuleUnknown:
            return QString("TWINLIFT_RULE_UNKNOWN");
    }
    Q_ASSERT(false);
    return QString("");
}

TwinLiftRules::Types stringToTwinLiftRules(QString string) {
    QString upper(string.toUpper());
    if (upper == "TWINLIFT_RULE_ALL") {
        return TwinLiftRules::TwinliftRuleAll;
    } else if (upper == "TWINLIFT_RULE_NOTHING") {
        return TwinLiftRules::TwinliftRuleNothing;
    } else if (upper == "TWINLIFT_RULE_BELOW") {
        return TwinLiftRules::TwinliftRuleBelow;
    } else {
        return TwinLiftRules::TwinliftRuleUnknown;
    }
}

}}  // namespace ange::schedule
