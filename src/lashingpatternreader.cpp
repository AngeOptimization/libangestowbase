#include "lashingpatternreader.h"

#include "referenceobject.h"

#include <ange/vessel/direction.h>
#include <ange/vessel/lashingpatterndata.h>

using namespace ange::vessel;
using namespace ange::vessel::Direction;
using namespace ange::units;

namespace ange {
namespace stowbase {

const LashingPatternData* readLashingPatternData(const Bundle& bundle, const ReferenceObject& vesselProfileRo) {
    ReferenceObject lashingPatternRo = vesselProfileRo.readOptionalReferenceObject("lashingPattern", bundle);
    if (lashingPatternRo.group() == "") {
        return 0;
    }

    QScopedPointer<LashingPatternData> lashingPatternData(new LashingPatternData());

    QList<ReferenceObject> patternRos = lashingPatternRo.readAllReferencedObjects("patterns", bundle);
    Q_FOREACH (const ReferenceObject& patternRo, patternRos) {
        /*-
         * Reading:
         *   }, {
         *     "reference" : "/object/4431",
         *     "object" : {
         *       "_group" : "stackLashingPattern",
         *        "angleInDegrees" : 41.0,
         *       "attachedToContainerNumber" : 2.0,
         *       "diameterInMeter" : 0.025,
         *       "eModuleInNewtonPerSquareMeter" : 140000.0,
         *       "maxLashingForceInNewton" : 230000.0,
         *       "minimumNumberOfContainersInStack" : 2.0,
         *       "patternName" : "Mid2P",
         *       "position" : "Bottom",
         *       "rodLengthInMeter" : 3.65,
         *       "side" : "Both"
         *     }
         *   }, {
         *     "reference" : "/object/4432",
         *     "object" : {
         *       "_group" : "stackLashingPattern",
         *       "attachedToContainerNumber" : 2.0,
         *       "diameterInMeter" : 0.025,
         *       "eModuleInNewtonPerSquareMeter" : 140000.0,
         *       "lashingDistanceTransverseInMeter" : 0.0,
         *       "lashingPlatePositionVerticalInMeter" : 32.413,
         *       "maxLashingForceInNewton" : 230000.0,
         *       "minimumNumberOfContainersInStack" : 2.0,
         *       "patternName" : "Mid2P",
         *       "position" : "Top",
         *       "side" : "Port"
         *     }
         *   }, {
         */
        QScopedPointer<StackLashingPattern> stackLashingPattern(new StackLashingPattern());

        stackLashingPattern->patternName = patternRo.readMandatoryValue<QString>("patternName");

        stackLashingPattern->minimumNumberOfContainersInStack =
            patternRo.readMandatoryValue<int>("minimumNumberOfContainersInStack");
        stackLashingPattern->minimumHcCountUnderLashing =
            patternRo.readOptionalValue<int>("minimumHcCountUnderLashing", 0);
        stackLashingPattern->attachedToContainerNumber =
            patternRo.readMandatoryValue<int>("attachedToContainerNumber");
        {
            bool ok;
            stackLashingPattern->position = toTopBottom(patternRo.readMandatoryValue<QString>("position"), &ok);
            if (!ok) {
                throw std::runtime_error("Invalid position in lashing");
            }
        }
        QString sideString = patternRo.readMandatoryValue<QString>("side");
        if (sideString == "Both") {
            stackLashingPattern->side << starboardPortValues();
        } else {
            bool ok;
            stackLashingPattern->side << toStarboardPort(sideString, &ok);
            if (!ok) {
                throw std::runtime_error("Invalid side in lashing");
            }
        }
        stackLashingPattern->eModule =
            patternRo.readMandatoryValue<double>("eModuleInNewtonPerSquareMeter") * newton / meter2;
        stackLashingPattern->diameter = patternRo.readMandatoryValue<double>("diameterInMeter") * meter;
        stackLashingPattern->maxLashingForce = patternRo.readMandatoryValue<double>("maxLashingForceInNewton") * newton;

        stackLashingPattern->lashingBridgeDeformation =
            patternRo.readOptionalValue<double>("lashingBridgeDeformationInMeter", 0.0) * meter;

        stackLashingPattern->angle = patternRo.readOptionalValue<double>("angleInDegrees", qQNaN()) / 180 * 3.141592;
        stackLashingPattern->rodLength = patternRo.readOptionalValue<double>("rodLengthInMeter", qQNaN()) * meter;
        stackLashingPattern->lashingPlatePostionVertical =
            patternRo.readOptionalValue<double>("lashingPlatePositionVerticalInMeter", qQNaN()) * meter;
        stackLashingPattern->lashingDistanceTransverse =
            patternRo.readOptionalValue<double>("lashingDistanceTransverseInMeter", qQNaN()) * meter;

        lashingPatternData->addStackPattern(stackLashingPattern.take());
    }

    QList<ReferenceObject> stackRos = lashingPatternRo.readAllReferencedObjects("stacks", bundle);
    Q_FOREACH (const ReferenceObject& stackRo, stackRos) {
        /*-
         * Reading:
         *   }, {
         *     "reference" : "/object/3290",
         *     "object" : {
         *       "_group" : "lashingStackData",
         *       "stack" : "urn:stowbase.org:vessel:bay=2,row=16,level=ABOVE",
         *       "stackPatternNameAft" : "Norm",
         *       "stackPatternNameFore" : "Low"
         *     }
         *   }, {
         *     "reference" : "/object/4231",
         *     "object" : {
         *       "_group" : "lashingStackData",
         *       "cellGuidesToTier" : 22.0,
         *       "stack" : "urn:stowbase.org:vessel:bay=70,row=1,level=ABOVE",
         *       "stackPatternNameAft" : "70-M",
         *       "stackPatternNameFore" : "70-M"
         *     }
         *   }, {
         */
        QString stackUrn = stackRo.readMandatoryValue<QString>("stack");
        BayRowLevel bayRowLevel = BayRowLevel::fromUrn(stackUrn);
        if (bayRowLevel.isUnknown()) {
            throw std::runtime_error("Invalid stack URN in lashing");
        }
        QString patternNameAft = stackRo.readMandatoryValue<QString>("stackPatternNameAft");
        QString patternNameFore = stackRo.readMandatoryValue<QString>("stackPatternNameFore");
        int cellGuidesToTier = stackRo.readOptionalValue<int>("cellGuidesToTier");
        lashingPatternData->addStackData(bayRowLevel, patternNameAft, patternNameFore, cellGuidesToTier);
    }

    return lashingPatternData.take();
}

}} // namespace ange::stowbase
