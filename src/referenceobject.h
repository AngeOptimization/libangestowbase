#ifndef LIBANGESTOWBASE_REFERENCE_OBJECT_H
#define LIBANGESTOWBASE_REFERENCE_OBJECT_H

#include <QString>
#include <QVariant>
#include "angestowbase_export.h"
#include "jsonutils.h"
#include <QSharedDataPointer>
#include <QStringList>

namespace ange {
namespace stowbase {

class Bundle;
}

  namespace containers {
    class IsoCode;
  }
}

namespace ange {
namespace stowbase {

class reference_object_private_t;

/**
 * A reference/object pair for a stowbase bundle
 * Implicitly shared
 */
class ANGESTOWBASE_EXPORT ReferenceObject {

  public:
    /**
    * Construct with group and reference initialized
    */
    ReferenceObject(QString group, QString reference);

    /**
    * Construct with group from a QVariantMap (which must contain a reference and an object with a  group)
    */
    explicit ReferenceObject(QVariantMap map) ;

    /**
     * Copy constructor
     */
    ReferenceObject(const ReferenceObject& other);

    /**
     * Assignment
     */
    const ReferenceObject& operator=(const ReferenceObject& rhs);

    ~ReferenceObject();

    /**
    * @return object
    */
    QVariantMap& object();

    /**
    * @return object (const version)
    */
    QVariantMap object() const;

    /**
    * @return reference
    */
    const QString& reference() const;

    /**
     * @return group name
     */
    QString group() const;

    /**
    * Convert to a map for QJson
    * You usually don't need this
    */
    QVariantMap toMap() const;

    // The following inserts are from http://kenai.com/projects/stowbase/pages/JSONTypes

    /**
     * Insert a object
     */
    void insertDouble(QString key, qreal number);

    /**
     * Insert a string
     */
    void insertString(QString key, QString string);

    /**
     * Insert a bool
     */
    void insertBool(QString key, bool value);

    /**
     * Insert a timestamp (not implemented)
     */

    /**
     * insert a list of strings
     */
    void insertStringList(QString key, QStringList list);

    /**
     * Inserts
     * @param stringlist with
     * @param name
     * into this
     */
    void insertStringList(QString name, QVariantList stringlist);

    /**
     * Insert a single reference
     */
    void insertReference(QString key, QVariant ref);

    /**
     * Insert a list of references
     */
    void insertReferences(QString key, QVariantList refs);

    /**
     * @return value for
     * @param key, casting an exception if  key is not found or is of the wrong type
     */
    template<typename T>
    T readMandatoryValue(const QString& key) const;

    /**
     * @return value for
     * @param key,
     * Returns a default constructed value if not found
     */
    template<typename T>
    T readOptionalValue(const QString& key) const;

    /**
     * @return reference_object_t reference by key.
     * Fails if multiple object or no object referenced
     */
    ReferenceObject readMandatoryReferenceObject(QString key, const Bundle& bundle) const;

    /**
     * @return reference_object_t reference by key, or an object with no group if not found
     */
    ReferenceObject readOptionalReferenceObject(QString key, const Bundle& bundle) const;

    /**
     * @return all reference_object_t's referenced by key. Never fails.
     */
    QList<ReferenceObject> readAllReferencedObjects(QString key, const Bundle& bundle) const;

    /**
     * @return reference for single-references
     */
    QString readOptionalReference(QString key) const;

    /**
     * @return value for
     * @param key,returning a copy of
     * @param def if not found
     */
    template<typename T>
    T readOptionalValue(const QString& key, const T& def) const;

    /**
     * @return true if empty (object() only contains the _group key)
     */
    bool empty() const;

  private:
    QVariant helperForReadMandatoryValue(const QString& key) const;

    QVariant helperForReadOptionalValue(const QString& key) const;

    QSharedDataPointer<reference_object_private_t> d;
};

template<typename T>
T ReferenceObject::readMandatoryValue(const QString& key) const {
  return vcast<T>(helperForReadMandatoryValue(key));
}

// Specialization for qreal that warns about nans
template<> ANGESTOWBASE_EXPORT
qreal ReferenceObject::readMandatoryValue<qreal>(const QString& key) const;

// Specialization for QStringList since they are not defined as straightforward
template<> ANGESTOWBASE_EXPORT
QStringList ReferenceObject::readMandatoryValue<QStringList>(const QString& key) const;


template<typename T>
T ReferenceObject::readOptionalValue(const QString& key) const {
  QVariant rv(helperForReadOptionalValue(key));
  return (rv.isValid() && rv.canConvert<T>()) ? vcast<T>(rv) : T();
}

template<typename T>
T ReferenceObject::readOptionalValue(const QString& key, const T& def) const {
  QVariant rv(helperForReadOptionalValue(key));
  return (rv.isValid() && rv.canConvert<T>()) ? vcast<T>(rv) : def;
}

// Specialization for QStringList since they are not defined as straightforward
template<> ANGESTOWBASE_EXPORT
QStringList ReferenceObject::readOptionalValue<QStringList>(const QString& key, const QStringList& def) const;

// Specialization for QStringList since they are not defined as straightforward
template<> ANGESTOWBASE_EXPORT
QStringList ReferenceObject::readOptionalValue<QStringList>(const QString& key) const;


ANGESTOWBASE_EXPORT QDebug operator<<(QDebug dbg, const ReferenceObject& o);


} // stowbase
} // ange

#endif
