#ifndef LIBANGESTOWBASE_CONTAINER_WRITER_H
#define LIBANGESTOWBASE_CONTAINER_WRITER_H

#include "angestowbase_export.h"
#include "bundle.h"
#include "jsonutils.h"

namespace ange {
  namespace containers {
    class Container;
  }
}

namespace ange {
namespace stowbase {
class ScheduleWriter;

class ReferenceObjectFactory;

class container_writer_private_t;

class ANGESTOWBASE_EXPORT ContainerWriter {

  public:

    /**
    * Simple constructor
    */
    ContainerWriter(Bundle& bundle);
    
    ~ContainerWriter();
    
    ContainerWriter(const ContainerWriter& orig);
    
    const ContainerWriter& operator=(const ContainerWriter& rhs);

    /**
    * Write a container to the bundle
    */
    QString operator()(const ange::containers::Container* container);

  private:
    QSharedDataPointer<container_writer_private_t> d;

};

} // stowbase
} // ange

#endif
