#ifndef LIBANGESTOWBASE_SCHEDULE_READER_H
#define LIBANGESTOWBASE_SCHEDULE_READER_H

#include <QVariant>
#include <stdexcept>
#include "jsonutils.h"
#include "angestowbase_export.h"

namespace ange {
namespace schedule {
class Schedule;
class Call;
}
}

namespace ange {
namespace stowbase {

class schedule_reader_private_t;
class Bundle;
class ReferenceObject;

class ANGESTOWBASE_EXPORT ScheduleReader {
  public:

    ScheduleReader();
    ~ScheduleReader();

    /**
    * Create schedule from JSON data.
    * @param device device to read the input from.
    */
    schedule::Schedule* readSchedule(QIODevice* device);

    /**
    * Create schedule from JSON data.
    * @param bundle the source bundle
    * @param schedule_ro the root schedule object
    */
    schedule::Schedule* readSchedule(const ange::stowbase::Bundle& bundle, const ange::stowbase::ReferenceObject& schedule_ro);

    /**
     * Map to schedule. Map containers into existing schedule, as far as possible
     */
    void mapToCurrentSchedule(const ange::stowbase::Bundle& bundle, const ange::stowbase::ReferenceObject& schedule_ro, const ange::schedule::Schedule& schedule);

    /**
    * Create schedule from JSON data (Legacy version)
    * @param bundle the source bundle
    */
    schedule::Schedule* readSchedule(const ange::stowbase::Bundle& bundle);

    /**
    * Read all calls
    */
    QList<schedule::Call*> readCalls(const ange::stowbase::Bundle& list);

    /**
    * Read a call
    */
    schedule::Call* readCall(const ange::stowbase::ReferenceObject& call_ro, const ange::stowbase::ReferenceObject& entry_ro);

    /**
     * Lookup call by reference (among already read calls)
     */
    const schedule::Call* call(const QString& reference) const;

  private:
    QScopedPointer<schedule_reader_private_t> d;
};


} // stowbase
} // ange

#endif
