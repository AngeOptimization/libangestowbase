#include "containerreader.h"
#include <stdexcept>
#include <ange/containers/dangerousgoodscode.h>
#include <ange/containers/container.h>
#include "referenceobject.h"
#include "bundle.h"
#include <ange/containers/oog.h>

using namespace ange::units;
using ange::containers::Container;

namespace ange {
namespace stowbase {

struct container_reader_private_t : public QSharedData {
  container_reader_private_t(const Bundle& bundle) : bundle(bundle) {}
  Bundle bundle;
};

ContainerReader::ContainerReader(const ange::stowbase::Bundle& bundle)
  : d(new container_reader_private_t(bundle))
{}

ContainerReader::ContainerReader(const ange::stowbase::ContainerReader& orig)
  : d(orig.d)
{}

const ange::stowbase::ContainerReader& ContainerReader::operator=(const ange::stowbase::ContainerReader & other) {
  if (this != &other) {
    d = other.d;
  }
  return *this;
}

ContainerReader::~ContainerReader()
{
  // Declared to handle shared
}

// Should be part of isocode library and have a test against the list of known lengths
static containers::IsoCode::IsoLength parseLength(QString lengthName) {
    if (lengthName == "urn:stowbase.org:container:length=20") {
        return containers::IsoCode::Twenty;
    } else if (lengthName == "urn:stowbase.org:container:length=40") {
        return containers::IsoCode::Fourty;
    } else if (lengthName == "urn:stowbase.org:container:length=45") {
        return containers::IsoCode::FourtyFive;
    } else if (lengthName == "urn:stowbase.org:container:length=53") {
        return containers::IsoCode::FiftyThree;
    } else {
        throw std::runtime_error(QString("Unknown lengthName '%1', (perhaps you miss isoCode on container)")
          .arg(lengthName).toStdString());
    }
}

// Should be part of isocode library and have a test against the list of known heights
static containers::IsoCode::IsoHeight parseHeight(QString heightName) {
    if (heightName == "urn:stowbase.org:height:dchc=DC") {
        return containers::IsoCode::DC;
    } else if (heightName == "urn:stowbase.org:height:dchc=HC") {
        return containers::IsoCode::HC;
    } else if (heightName == "urn:stowbase.org:height:dchc=HALF_LC") {
        return containers::IsoCode::HalfLC;
    } else if (heightName == "urn:stowbase.org:height:dchc=HALF_DC") {
        return containers::IsoCode::HalfDC;
    } else if (heightName == "urn:stowbase.org:height:dchc=LC") {
        return containers::IsoCode::LC;
    } else if (heightName == "urn:stowbase.org:height:dchc=MC") {
        return containers::IsoCode::MC;
    } else {
        throw std::runtime_error(QString("Unknown heightName '%1', (perhaps you miss isoCode on container)")
          .arg(heightName).toStdString());
    }
}

ange::containers::Container* ContainerReader::read(const ange::stowbase::ReferenceObject& container) {
  const bool isLiveReefer = container.readMandatoryValue<bool>("isLiveReefer");

  QString isoString = container.readOptionalValue<QString>("isoCode");
  if (!isoString.isEmpty()) {
    const QString prefix("urn:stowbase.org:container:iso=");
    if (isoString.startsWith(prefix)) {
      isoString.remove(0, prefix.length());
    } else {
      throw std::runtime_error(QString("Invalid isoCode '%1'").arg(isoString).toStdString());
    }
  } else {
    isoString = container.readOptionalValue<QString>("rawIsoCode");
  }
  ange::containers::IsoCode iso("42G1");
  if (!isoString.isEmpty()) {
    iso = ange::containers::IsoCode(isoString);
  } else {
    ange::containers::IsoCode::IsoLength length = parseLength(container.readOptionalValue<QString>("lengthName"));
    ange::containers::IsoCode::IsoHeight height = parseHeight(container.readOptionalValue<QString>("heightName"));
    iso = ange::containers::IsoCode(length, height, isLiveReefer);
  }

  // urn:stowbase.org:container:id=ABCU1234565
  QString equipmentId = readContainerId(container);
  qreal reeferTemperature = container.readOptionalValue<qreal>("reeferTemperature", Container::unknownTemperature());
  const bool empty = container.readMandatoryValue<bool>("isEmpty");
  const QString placeOfDelivery = container.readOptionalValue<QString>("placeOfDelivery");
  QStringList rawimocodes(container.readOptionalValue<QStringList>("dangerousGoods"));
  QString carrier_code = container.readOptionalValue<QString>("carrierCode");
  QString booking_number = container.readOptionalValue<QString>("bookingNumber");
    ange::containers::Oog oog;
    const qreal oogFrontCm = container.readOptionalValue<qreal>("oogFrontCm", 0.0);
    if (!qFuzzyIsNull(oogFrontCm)) {
        oog.setFront(oogFrontCm * centimeter);
    }
    const qreal oogBackCm = container.readOptionalValue<qreal>("oogBackCm", 0.0);
    if (!qFuzzyIsNull(oogBackCm)) {
        oog.setBack(oogBackCm * centimeter);
    }
    const qreal oogLeftCm = container.readOptionalValue<qreal>("oogLeftCm", 0.0);
    if (!qFuzzyIsNull(oogLeftCm)) {
        oog.setLeft(oogLeftCm * centimeter);
    }
    const qreal oogRightCm = container.readOptionalValue<qreal>("oogRightCm", 0.0);
    if (!qFuzzyIsNull(oogRightCm)) {
        oog.setRight(oogRightCm * centimeter);
    }
    const qreal oogTopCm = container.readOptionalValue<qreal>("oogTopCm", 0.0);
    if (!qFuzzyIsNull(oogTopCm)) {
        oog.setTop(oogTopCm * centimeter);
    }
  QList<ange::containers::DangerousGoodsCode> dgs;
  //keeping the (:?,imdgClass=\\S+) for backwards compatibility, even though we no longer use it
  //as the IMO classes are given by the UN number
  QRegExp matcher("^\\s*urn:stowbase.org:dg:unNumber=(\\S+)(:?,imdgClass=\\S+)?\\s*$");
  Q_FOREACH(QString rawcode, rawimocodes) {
    if (matcher.exactMatch(rawcode)) {
      QString un_number = matcher.cap(1);
      dgs << ange::containers::DangerousGoodsCode(un_number);
    }
  }
  Mass weight = container.readMandatoryValue<qreal>("grossWeightInKg")*kilogram;
  QList<ange::containers::HandlingCode> handlingCodes;
  {
    QStringList handling_codes_stringlist = container.readOptionalValue<QStringList>("handlingCodes");
    Q_FOREACH(const QString& handling_code_string,handling_codes_stringlist) {
        handlingCodes << ange::containers::HandlingCode(handling_code_string);
    }
  }
    Container* container2 = new Container(equipmentId, weight, iso);
    container2->setLive(isLiveReefer ? Container::Live : Container::NonLive);
    container2->setTemperature(reeferTemperature);
    container2->setEmpty(empty);
    container2->setDangerousGoodsCodes(dgs);
    container2->setOog(oog);
    container2->setBookingNumber(booking_number);
    container2->setCarrierCode(carrier_code);
    container2->setPlaceOfDelivery(placeOfDelivery);
    container2->setHandlingCodes(handlingCodes);
    return container2;
}

QString ContainerReader::readContainerId(const ange::stowbase::ReferenceObject& container) {
  const QString containerId = container.readOptionalValue<QString>("containerId");
  if (containerId.startsWith("urn:stowbase.org:container:id=")) {
    return containerId.right(containerId.length() - 30);
  } else if (containerId == "urn:stowbase.org:container:non-standard") {
    return container.readMandatoryValue<QString>("containerIdNonStandard");
  } else if (containerId.isEmpty()) {
    return containerId;
  } else {
    throw std::runtime_error(QString("invalid containerId, invalid JSON data: '%1'").arg(containerId).toStdString());
  }
}

containers::Container* ContainerReader::read(const QString& container_reference) {
  ReferenceObject ro = d->bundle.reference(container_reference);
  return read(ro);
}

} // stowbase
} // ange
