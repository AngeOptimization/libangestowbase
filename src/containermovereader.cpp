/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include <QRegExp>

#include <algorithm>

#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <ange/containers/container.h>
#include <ange/containers/dangerousgoodscode.h>

#include <ange/vessel/slot.h>
#include <ange/vessel/vessel.h>

#include "containermovereader.h"
#include "schedulereader.h"
#include <QStringBuilder>

using ange::schedule::Call;
using ange::containers::Container;
using ange::vessel::Slot;
using ange::schedule::Schedule;
using ange::vessel::Vessel;

namespace ange {
namespace stowbase {

struct container_move_reader_private_t : public QSharedData {
  container_move_reader_private_t(const ange::stowbase::Bundle& bundle, const Vessel* vessel, const ScheduleReader* schedule_reader);
  const ange::stowbase::Bundle bundle;
  const ange::vessel::Vessel* vessel;
  const ScheduleReader* schedule_reader;
};

container_move_reader_private_t::container_move_reader_private_t(const ange::stowbase::Bundle& bundle, const ange::vessel::Vessel* vessel, const ange::stowbase::ScheduleReader* schedule_reader)
  : bundle(bundle),
    vessel(vessel),
    schedule_reader(schedule_reader)
{
}


ContainerMoveReader::ContainerMoveReader(const ange::stowbase::Bundle bundle,
                                                 const ange::vessel::Vessel* vessel,
                                                 const ScheduleReader* schedule_reader) :
    d(new container_move_reader_private_t(bundle, vessel, schedule_reader))
{
}

ContainerMoveReader::Move ContainerMoveReader::read(const ange::stowbase::ReferenceObject& move_ro) {
  QString from_urn = move_ro.readOptionalValue<QString>("from");
  QString to_urn = move_ro.readOptionalValue<QString>("to");
  QString uncode = get_uncode(from_urn, to_urn);
  const Slot* slot = 0L;
  const Call* call = 0L;
  bool is_pseudo = false;
  QRegExp toMatcher("urn:stowbase.org:vessel:imo=(.*),bay=(\\d+),row=(\\d+),tier=(\\d+)");
  if (to_urn.startsWith("urn:stowbase.org:vessel:imo=")) {
    // urn:stowbase.org:vessel:imo=1000000,bay=42,row=8,tier=86
    if (toMatcher.exactMatch(to_urn)) {
      ange::vessel::BayRowTier brt = ange::vessel::BayRowTier(toMatcher.cap(2), toMatcher.cap(3), toMatcher.cap(4));
      slot = d->vessel->slotAt(brt);
      if (!slot) {
        throw std::runtime_error(QString(QString::fromUtf8("Tried to load container in non-existing slot: ") % brt.toString() %
        QString::fromUtf8(".  Please be sure that the profile of the vessel corresponds to the vessel where containers are to be loaded.")).toStdString());
      }
    } else {
      slot = ange::vessel::UnknownPosition;
      is_pseudo = true;
    }
  }
  if (from_urn.startsWith("urn:stowbase.org:vessel:imo=") && !toMatcher.exactMatch(from_urn)) {
    is_pseudo = true;
  }
  QString call_reference = move_ro.readOptionalReference("call");
  if (d->schedule_reader && !call_reference.isEmpty()) {
    call = d->schedule_reader->call(call_reference);
  }

  QStringList cargo;
  Q_FOREACH(QVariant cargo_reference, move_ro.readMandatoryValue<QVariantList>("cargo")) {
    cargo << cargo_reference.toString();
  }
  return Move(cargo, uncode, call, slot, is_pseudo, move_ro);
}

QString ContainerMoveReader::get_uncode(const QString& from_urn, const QString& to_urn) const {
  static const char* const port_loc_json = "urn:stowbase.org:port:unlocode=";
  if (from_urn.startsWith(port_loc_json)) {
    QString uncode(from_urn);
    uncode.remove(port_loc_json);
    return uncode;
  } else if (to_urn.startsWith(port_loc_json)) {
    QString uncode(to_urn);
    uncode.remove(port_loc_json);
    return uncode;
  }
  return QString();

}

struct ContainerMoveReader::move_private_t {
  ReferenceObject m_raw_object;
  move_private_t(const ReferenceObject& raw_object) :
      m_raw_object(raw_object) {
  }
};

ContainerMoveReader::Move::Move(const ange::stowbase::ContainerMoveReader::Move& move) :
  cargo(move.cargo),
  call(move.call),
  slot(move.slot),
  uncode(move.uncode),
  isPseudo(move.isPseudo),
  d(move.d ? new move_private_t(*move.d) : 0L)
{
}

const ange::stowbase::ContainerMoveReader::Move& ContainerMoveReader::Move::operator=(const ange::stowbase::ContainerMoveReader::Move & rhs)
{
  if (this != &rhs) {
    cargo = rhs.cargo;
    call = rhs.call;
    slot = rhs.slot;
    uncode = rhs.uncode;
    isPseudo = rhs.isPseudo;
    delete d;
    d = rhs.d ? new move_private_t(*rhs.d) : 0L;
  }
  return *this;
}

const ange::stowbase::ReferenceObject& ContainerMoveReader::Move::rawObject() const {
  return d->m_raw_object;
}

ContainerMoveReader::Move::~Move() {
  delete d;
}

ContainerMoveReader::Move::Move(QStringList cargo,
                                        const QString& uncode,
                                        const ange::schedule::Call* call,
                                        const ange::vessel::Slot* slot,
                                        bool is_pseudo,
                                        const ReferenceObject& raw_object)
  : cargo(cargo),
    call(call),
    slot(slot),
    uncode(uncode),
    isPseudo(is_pseudo),
    d(new move_private_t(raw_object)){}

QDebug operator<<(QDebug dbg, const ange::stowbase::ContainerMoveReader::Move& rhs) {
  return dbg << "[" << rhs.cargo << ", " << rhs.uncode << "/" << rhs.call << "," << rhs.slot << " " << (rhs.isPseudo?"P":"") << "]";
}


const ange::stowbase::ContainerMoveReader& ContainerMoveReader::operator=(const ange::stowbase::ContainerMoveReader & rhs) {
  if (this != &rhs) {
    d = rhs.d;
  }
  return *this;
}

ContainerMoveReader::ContainerMoveReader(const ange::stowbase::ContainerMoveReader& orig) : d(orig.d) {
}

ContainerMoveReader::~ContainerMoveReader() {
}

}} // end of namespace
