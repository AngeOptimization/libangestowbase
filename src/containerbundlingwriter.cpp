/*
 *
 */

#include "containerbundlingwriter.h"
#include "referenceobject.h"
#include "bundle.h"
#include <QSharedData>

namespace ange {
namespace stowbase {


using ange::stowbase::ReferenceObject;

class ContainerBundlingWriterPrivate : public QSharedData {
    public:
        ContainerBundlingWriterPrivate(ange::stowbase::Bundle& bundle) : m_bundle(bundle)  {

        }
        ange::stowbase::Bundle& m_bundle;
};

ContainerBundlingWriter::ContainerBundlingWriter(ange::stowbase::Bundle& bundle) : d(new ContainerBundlingWriterPrivate(bundle)) {

}

ContainerBundlingWriter::ContainerBundlingWriter(const ange::stowbase::ContainerBundlingWriter& other) : d(other.d){

}

ContainerBundlingWriter& ange::stowbase::ContainerBundlingWriter::operator=(const ange::stowbase::ContainerBundlingWriter& other) {
    if (&other != this) {
        d = other.d;
    }
    return *this;
}

ContainerBundlingWriter::~ContainerBundlingWriter() {
    //nothing
}



QString ContainerBundlingWriter::operator()(const QString& containerReference, const QStringList& bundledContainers) {
    ReferenceObject bundling_ro = d->m_bundle.build("containerBundling");

    bundling_ro.object().insert("bottomContainer",containerReference);
    bundling_ro.insertStringList("bundledContainers", bundledContainers);

    d->m_bundle << bundling_ro;
    return bundling_ro.reference();
}

} // namespace stowbase
} // namespace ange

