
#include "jsonutils.h"

#include <qjson/parser.h>
#include <qjson/serializer.h>
#include <ange/containers/isocode.h>
#include <QRegExp>
#include <QFile>
#include <QTextStream>
#include <QMimeDatabase>
#include <KF5/KArchive/KCompressionDevice>

namespace ange {
namespace stowbase {

QVariant json_read(QIODevice* device) {
  QJson::Parser parser;
  parser.allowSpecialNumbers(true);
  bool ok;
  KCompressionDevice compressor(device, false, KCompressionDevice::GZip);
  bool compressed_ok = compressor.open(QIODevice::ReadOnly);
  QMimeDatabase db;
  QMimeType type = db.mimeTypeForData(device);
  if(type.inherits(QString("application/x-gzip"))) {
      Q_ASSERT(compressed_ok);
  }
  const QVariant json = compressed_ok ? parser.parse(&compressor, &ok) : parser.parse(device, &ok);
  if (not ok) {
    throw std::runtime_error(("Syntax error in JSON input line " + QString::number(parser.errorLine()) + ": " + parser.errorString()).toStdString());
  }
  return json;
}

QVariant json_read(const QString& file_name) {
  QFile file(file_name);
  if (not file.open(QIODevice::ReadOnly)) {
    throw std::runtime_error(QString("Failed to open file '%1', error was '%2'").arg(file_name).arg(file.errorString()).toStdString());
  }
  return json_read(&file);
}

void json_write(const QVariant& data, QIODevice* device) {
  KCompressionDevice compressor(device, false, KCompressionDevice::GZip);
  if (!compressor.open(QIODevice::WriteOnly)) {
    throw std::runtime_error(QString("Failed to open file for writing: %1").arg(compressor.errorString()).toStdString());
  }
  json_write_uncompressed(data,&compressor);
}

void json_write(const QVariant& data, const QString& file_name) {
  QFile file(file_name);
  if (not file.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
    throw std::runtime_error(QString("Failed to open file '%1' for saving.\nError: %2").arg(file_name).arg(file.errorString()).toStdString());
  }
  json_write(data, &file);
  if (!file.flush()) {
    throw std::runtime_error("Failed to save file --- disk full?");
  }
}

void json_write_uncompressed(const QVariant& data, QIODevice* device) {
  Q_ASSERT(device->isOpen());
  QJson::Serializer json_serializer;
  json_serializer.allowSpecialNumbers(true);
  json_serializer.setIndentMode(QJson::IndentFull);
  QTextStream stream(device);
  QByteArray output = json_serializer.serialize(data).data();
  if (output.isNull() || output.isEmpty()) {
    throw std::runtime_error("Failed to serialize data to JSON");
  }
  stream << output;
  stream.flush();
}


QString vessel_to_urn (QString imo) {
  return QString("urn:stowbase.org:vessel:imo=%1").arg(imo);
}

QString vessel_position_to_urn(QString imo, int bay, int row, int tier) {
  return QString("urn:stowbase.org:vessel:imo=%1,bay=%2,row=%3,tier=%4").arg(imo).arg(bay).arg(row).arg(tier);
}

QString vessel_stack_position_to_urn(QString imo, int bay, int row, ange::vessel::DeckLevel level) {
  return QString("urn:stowbase.org:vessel:imo=%1,bay=%2,row=%3,level=%4").arg(imo).arg(bay).arg(row).arg(ange::vessel::toChar(level));
}

bool urn_to_vessel_stack_position(QString urn, QString& imo, int& bay, int& row, ange::vessel::DeckLevel& level) {
  QRegExp toMatcher("urn:stowbase.org:vessel:imo=(.+),bay=(\\d+),row=(\\d+),level=(A|B)");
  // urn:stowbase.org:vessel:imo=1000000,bay=42,row=8,tier=86
  if (toMatcher.exactMatch(urn)) {
    imo=toMatcher.cap(1);
    bay=toMatcher.cap(2).toInt();
    row=toMatcher.cap(3).toInt();
    level = (toMatcher.cap(4) == "A" ? ange::vessel::Above : ange::vessel::Below);
    return true;
  }

  imo="unknown";
  bay=0;
  row=0;
  level = ange::vessel::Above;
  return false;

}

QString port_from_urn(const QString urn) {
  const QString prefix("urn:stowbase.org:port:unlocode=");
  if (urn.startsWith(prefix)) {
    return urn.mid(prefix.length());
  } else {
    throw std::runtime_error(QString("not jsonString.startsWith(\"%2\"), invalid JSON data: '%1'")
      .arg(urn).arg(prefix).toStdString());
  }
}

QString port_to_urn(QString uncode) {
  return QString("urn:stowbase.org:port:unlocode=%1").arg(uncode);
}

QString IsoCodeo_urn(const ange::containers::IsoCode& isocode) {
  return QString::fromLocal8Bit("urn:stowbase.org:container:iso=%1").arg(isocode.code());
}

QString container_id_to_urn(const QString& container_id) {
  return QString::fromLocal8Bit("urn:stowbase.org:container:id=%1").arg(container_id);
}

void set_container_id(QVariantMap& object, const QString& container_id) {
  object.insert("containerId", container_id_to_urn(container_id));
}

} // stowbase
} // ange
