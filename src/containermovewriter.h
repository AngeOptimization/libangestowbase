/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010
 Copyright: See COPYING file that comes with this distribution
*/

#ifndef CONTAINER_MOVE_WRITER_H
#define CONTAINER_MOVE_WRITER_H

#include <QSharedDataPointer>
#include <ange/vessel/slot.h>
#include <ange/schedule/call.h>

#include "angestowbase_export.h"
#include "referenceobject.h"

namespace ange {
namespace containers {
class Container;
}

namespace stowbase {
class ReferenceObjectFactory;
class ScheduleWriter;
class container_move_writer_private_t;

class ANGESTOWBASE_EXPORT ContainerMoveWriter {
  public:
    ContainerMoveWriter(ange::stowbase::Bundle& out, const ange::stowbase::ScheduleWriter* schedule_writer);
    ~ContainerMoveWriter();
    ContainerMoveWriter(const ContainerMoveWriter& orig);

    /**
     * Write load
     * @param cargo_reference reference to cargo object in bundle
     * @param vessel vessel that container is discharge from (cannot be null)
     * @param brt position of container
     * @param call call where this move takes place
     */
    QString writeLoad(QString cargo_reference,
                       const ange::vessel::Vessel* vessel,
                       const ange::vessel::BayRowTier brt,
                       const ange::schedule::Call* call,
                       const QString& nomiel_call_uncode);

    /**
     * Build a load object and return the object without adding to bundle.
     * Useful for adding extra data.
     * Works same as build_load
     */
    ReferenceObject buildLoad(QString cargo_reference,
                                  const ange::vessel::Vessel* vessel,
                                  const ange::vessel::BayRowTier brt,
                                  const ange::schedule::Call* call,
                                  const QString& nomiel_call_uncode);

    /**
     * Write discharge
     * @param cargo_reference reference to cargo object in bundle
     * @param vessel vessel that container is discharge from (cannot be null)
     * @param brt position of container
     * @param call call where this move takes place
     */
    QString writeDischarge(QString cargo_reference,
                            const ange::vessel::Vessel* vessel,
                            const ange::vessel::BayRowTier brt,
                            const ange::schedule::Call* call,
                            const QString& nomiel_call_uncode);

    /**
     * Build a discharge object and return the object without adding to bundle.
     * Useful for adding extra data.
     * Works same as build_discharge
     */
    ReferenceObject buildDischarge(QString cargo_reference,
                                       const ange::vessel::Vessel* vessel,
                                       const ange::vessel::BayRowTier brt,
                                       const ange::schedule::Call* call,
                                       const QString& nomiel_call_uncode);

  private:
    QSharedDataPointer<container_move_writer_private_t> d;
};
}
}

#endif // CONTAINER_MOVE_WRITER_H
