#ifndef LIBANGESTOWBASE_CONTAINER_READER_H
#define LIBANGESTOWBASE_CONTAINER_READER_H

#include "angestowbase_export.h"
#include <QString>
#include <QSharedDataPointer>

namespace ange {
namespace containers {

class Container;
}

namespace stowbase {

class ReferenceObject;


class Bundle;
class container_reader_private_t;

class ANGESTOWBASE_EXPORT ContainerReader {

  public:
    ContainerReader(const Bundle& bundle);
    ContainerReader(const ContainerReader& orig);
    ~ContainerReader();
    const ContainerReader& operator=(const ContainerReader& other);

    /**
     * @return container read from container.
     */
    ange::containers::Container* read(const ReferenceObject& container);

    /**
     * @return container with reference
     * @param reference
     */
    ange::containers::Container* read(const QString& container_reference);
  private:
    QString readContainerId(const ange::stowbase::ReferenceObject& container);
    QSharedDataPointer<container_reader_private_t> d;
};

} // stowbase
} // ange

#endif
