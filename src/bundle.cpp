#include "bundle.h"

#include <QTextStream>
#include "jsonutils.h"
#include <QFile>

namespace ange {
namespace stowbase {

class Bundle::BundlePrivate : public QSharedData {
  public:
    BundlePrivate(int counter_value = 0);
    QList<ReferenceObject> objects;
    QHash<QString, int> index;
    int m_new_object_counter;

    /**
     * Read all data from QVariantList of JSON objects
     */
    void read(QVariantList list);

};

Bundle::BundlePrivate::BundlePrivate(int counter_value)
    : QSharedData(), m_new_object_counter(counter_value)
{
    // Empty
}

void Bundle::write(QIODevice* device)  const {
  QVariantMap map;
  map.insert("hasNext", false);
  QVariantList data;
  Q_FOREACH(const ReferenceObject& o, d->objects) {
    data << o.toMap();
  }
  map.insert("data", data);
  json_write(map, device);
}

void Bundle::write_uncompressed(QIODevice* device) const {
  QVariantMap map;
  map.insert("hasNext", false);
  QVariantList data;
  Q_FOREACH(const ReferenceObject& o, d->objects) {
    data << o.toMap();
  }
  map.insert("data", data);
  json_write_uncompressed(map, device);
}


void Bundle::write(QString filename) const {
  QFile file(filename);
  if (!file.open(QIODevice::WriteOnly)) {
    throw std::runtime_error(QString("Failed to open file '%1' for writing: %2").arg(filename).arg(file.errorString()).toStdString());
  }
  write(&file);
}

void Bundle::BundlePrivate::read(QVariantList list) {
  Q_FOREACH(QVariant elem, list) {
    if (elem.type() == QVariant::Map) {
      ReferenceObject object(elem.toMap());
      if (!index.contains(object.reference())) {
        index.insert(object.reference(), objects.size());
        objects << object;
        bool ok;
        const int int_reference = object.reference().toInt(&ok);
        if (ok) {
          m_new_object_counter = std::max(int_reference, m_new_object_counter+1);
        }
      } else {
        qWarning("Attempt to overwrite object with reference %s", object.reference().toLocal8Bit().data());
      }
    } else {
      qWarning("Unexpected element in list of type %s", elem.typeName());
    }
  }
}

void Bundle::read(QIODevice* device) {
  QVariant raw = json_read(device);
  if (raw.type() == QVariant::Map) {
    d->read(vcast<QVariantList>(raw.toMap().value("data")));
  } else if (raw.type() == QVariant::List) {
    d->read(raw.toList());
  } else {
    throw std::runtime_error("Unrecognized json format");
  }
}

ReferenceObject Bundle::find(QString object_type, QVariantMap attributes) const {
  ReferenceObject ro_found("", "");
  Q_FOREACH(const ReferenceObject& object, d->objects) {
    if (object.group() == object_type) {
      bool match = true;
      for (QVariantMap::const_iterator it = attributes.begin(), iend = attributes.end(); it != iend; ++it) {
        if (!object.object().contains(it.key()) || object.object().value(it.key()) != it.value()) {
          match = false;
          break;
        }
      }
      if (match) {
        if (!ro_found.group().isEmpty()) {
          QByteArray attribute_as_text;
          QTextStream out(&attribute_as_text);
          out << "{";
          for (QVariantMap::const_iterator it = attributes.begin(), iend = attributes.end(); it != iend; ++it) {
            out << it.key() << "=\"" << it.value().toString() << "\", ";
          }
          out << "}";
          qWarning("Multiple reference matched object type %s and keys %s", object_type.toLocal8Bit().data(), attribute_as_text.data());
        }
        ro_found = object;
      }
    }
  }
  return ro_found;
}

void Bundle::operator<<(const ange::stowbase::ReferenceObject& object) {
  if (d->index.contains(object.reference())) {
    throw std::runtime_error(QString("bundle already contains object with reference \"%1\"").arg(object.reference()).toStdString());
  }
  d->index.insert(object.reference(), d->objects.size());
  d->objects << object;
  bool ok;
  const int int_reference = object.reference().toInt(&ok);
  if (ok) {
    d->m_new_object_counter = std::max(d->m_new_object_counter, int_reference+1);
  }
}

ReferenceObject Bundle::reference(QString reference) const {
  int index = d->index.value(reference, -1);
  if (index == -1) {
    qWarning("Looking for reference '%s' which was not found", reference.toLocal8Bit().data());
    return ReferenceObject("", "");
  }
  return d->objects.at(index);
}

ReferenceObject Bundle::reference(QVariant ref) const {
  return reference(ref.toString());
}

Bundle::Bundle() : d(new BundlePrivate())
{

}

Bundle::Bundle(const ange::stowbase::Bundle& other) : d(other.d) {

}

void Bundle::clear() {
  d->index.clear();
  d->objects.clear();
  d->m_new_object_counter = 0;
}

Bundle::~Bundle()
{
  // Declared to have BundlePrivate's destructor in scope
}
Bundle::iterator Bundle::begin() {
  return d->objects.begin();
}

Bundle::const_iterator Bundle::begin() const {
  return d->objects.begin();
}

Bundle::iterator Bundle::end() {
  return d->objects.end();
}

Bundle::const_iterator Bundle::end() const {
  return d->objects.end();

}
bool Bundle::empty() const {
  return d->objects.empty();
}

const ange::stowbase::Bundle& Bundle::operator=(const ange::stowbase::Bundle & rhs) {
  if (this != &rhs) {
    d = rhs.d;
  }
  return *this;
}

ReferenceObject Bundle::build(const QString& group) {
  ReferenceObject ro(group, QString("/object/%1").arg(d->m_new_object_counter++));
  return ro;

}

} // stowbase
} // ange
