/*
 *
 */

#ifndef ANGE_STOWBASE_CONTAINERBUNDLINGREADER_H
#define ANGE_STOWBASE_CONTAINERBUNDLINGREADER_H

#include <QSharedDataPointer>
#include <QPair>
#include <QStringList>

#include "angestowbase_export.h"

namespace ange {
namespace stowbase {

class Bundle;
class ReferenceObject;

class ContainerBundlingReaderPrivate;

class ANGESTOWBASE_EXPORT ContainerBundlingReader {
    public:
        typedef QPair<QString,QStringList> ContainerBundlingData;
        ContainerBundlingReader(const Bundle& json_bundle);
        ContainerBundlingReader(const ContainerBundlingReader& other);
        ~ContainerBundlingReader();
        ContainerBundlingReader& operator=(const ContainerBundlingReader& other);
        /**
         * @return container read from container.
         */
        ContainerBundlingData read(const ReferenceObject& containerBundling);

        /**
         * @return container with reference
         * @param reference
         */
        ContainerBundlingData read(const QString& containerBundlingReference);

    private:
        QSharedDataPointer<ContainerBundlingReaderPrivate> d;
};
} // namespace stowbase
} // namespace ange

#endif // ANGE_STOWBASE_CONTAINERBUNDLINGREADER_H
