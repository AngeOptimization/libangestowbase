#include "containerwriter.h"

#include "jsonutils.h"
#include "referenceobject.h"

#include <ange/containers/container.h>
#include <ange/containers/dangerousgoodscode.h>
#include <ange/containers/oog.h>

using namespace ange::units;

namespace ange {
namespace stowbase {

struct container_writer_private_t : public QSharedData {
  container_writer_private_t(ange::stowbase::Bundle& bundle);
  Bundle& bundle;
};

container_writer_private_t::container_writer_private_t(ange::stowbase::Bundle& bundle):
    QSharedData(),
    bundle(bundle)
{}

ContainerWriter::ContainerWriter(const ange::stowbase::ContainerWriter& orig) : d(orig.d)
{}

const ange::stowbase::ContainerWriter& ContainerWriter::operator=(const ange::stowbase::ContainerWriter& rhs) {
  if (&rhs != this) {
    d = rhs.d;
  }
  return *this;
}

ContainerWriter::~ContainerWriter() {
}

ContainerWriter::ContainerWriter(ange::stowbase::Bundle& bundle) :
    d(new container_writer_private_t(bundle)) {
  // nothing
}

QString ContainerWriter::operator()(const ange::containers::Container* container) {
  ReferenceObject container_ro = d->bundle.build("cargo");
  // The fields are described in http://kenai.com/projects/stowbase/pages/Resource-Cargo
  // Common fields
  container_ro.object().insert("grossWeightInKg", container->weight()/kilogram);
  // Container fields
  set_container_id(container_ro.object(), container->equipmentNumber());
  container_ro.object().insert("isEmpty", container->empty());
  container_ro.object().insert("isoCode", IsoCodeo_urn(container->isoCode()));
  container_ro.object().insert("isLiveReefer", container->live() == containers::Container::Live);
    if(container->live() == ange::containers::Container::Live) {
        container_ro.object().insert("reeferTemperature", container->temperature());
    }
  if(!container->carrierCode().isEmpty()){
    container_ro.object().insert("carrierCode", container->carrierCode());
  }
  if(!container->bookingNumber().isEmpty()){
    container_ro.object().insert("bookingNumber", container->bookingNumber());
  }
  if(!container->placeOfDelivery().isEmpty()){
    container_ro.object().insert("placeOfDelivery", container->placeOfDelivery());
  }
    if (!qFuzzyIsNull(container->oog().back() / centimeter)) {
        container_ro.object().insert("oogBackCm", container->oog().back() / centimeter);
    }
    if (!qFuzzyIsNull(container->oog().front() / centimeter)) {
        container_ro.object().insert("oogFrontCm", container->oog().front() / centimeter);
    }
    if (!qFuzzyIsNull(container->oog().left() / centimeter)) {
        container_ro.object().insert("oogLeftCm", container->oog().left() / centimeter);
    }
    if (!qFuzzyIsNull(container->oog().right() / centimeter)) {
        container_ro.object().insert("oogRightCm", container->oog().right() / centimeter);
    }
    if (!qFuzzyIsNull(container->oog().top() / centimeter)) {
        container_ro.object().insert("oogTopCm", container->oog().top() / centimeter);
    }
  if (container->isDangerous()) {
    QStringList dgs;
    Q_FOREACH(const ange::containers::DangerousGoodsCode& dg, container->dangerousGoodsCodes()) {
      dgs << QString("urn:stowbase.org:dg:unNumber=%1").arg(dg.unNumber() + (dg.isLimitedQuantity()?"L":""));
    }
    container_ro.insertStringList("dangerousGoods", dgs);
  }
  QList<ange::containers::HandlingCode> handling_codes = container->handlingCodes();
  if (!handling_codes.isEmpty()) {
        QStringList handling_code_stringlist;
        Q_FOREACH(const ange::containers::HandlingCode& handling_code, handling_codes) {
            handling_code_stringlist << handling_code.code();
        }
        container_ro.insertStringList("handlingCodes", handling_code_stringlist);
  }
  d->bundle << container_ro;
  return container_ro.reference();
}

} // stowbase
} // ange
