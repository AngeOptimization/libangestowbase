/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "containermovewriter.h"
#include "schedulewriter.h"
#include <ange/containers/isocode.h>
#include <ange/vessel/vessel.h>
#include <ange/containers/container.h>
#include <ange/vessel/bayslice.h>

#include <stdexcept>

namespace ange {
namespace stowbase {
class container_move_writer_private_t  : public QSharedData {
public:
    container_move_writer_private_t(Bundle& bundle, const ScheduleWriter* schedule_writer)
      : bundle(bundle),
        schedule_writer(schedule_writer)
    {
    }
  Bundle& bundle;
  const ScheduleWriter* schedule_writer;
};

ContainerMoveWriter::ContainerMoveWriter(ange::stowbase::Bundle& out, const ange::stowbase::ScheduleWriter* schedule_writer)
  : d(new container_move_writer_private_t(out, schedule_writer))
{

}

ContainerMoveWriter::ContainerMoveWriter(const ContainerMoveWriter& orig)
    : d(orig.d)
{

}
ContainerMoveWriter::~ContainerMoveWriter()
{

}

namespace {

static
QString urn(const vessel::BayRowTier brt, const vessel::Vessel* vessel) {
  QString pos;
  if (not brt.placed()) {
    pos = vessel_to_urn(vessel->imoNumber());
  } else {
    pos = vessel_position_to_urn(vessel->imoNumber(), brt.bay(), brt.row(), brt.tier());
  }
  return pos;
}
}


ReferenceObject ContainerMoveWriter::buildDischarge(QString cargo_reference,
                                                            const ange::vessel::Vessel* vessel,
                                                            const ange::vessel::BayRowTier brt,
                                                            const ange::schedule::Call* call,
                                                            const QString& nomiel_call_uncode)
{
  ReferenceObject move_ro = d->bundle.build("move");
  move_ro.insertReference("cargo", cargo_reference);
  move_ro.insertString("from", urn(brt, vessel));
  if (call) {
    if (d->schedule_writer) {
      QString call_reference = d->schedule_writer->reference(call);
      if (call_reference.isEmpty()) {
        qWarning("Internal inconsistency: %s is not in schedule, but still had moves for cargo %s.", call->uncode().toLocal8Bit().data(), cargo_reference.toLocal8Bit().data());
      } else {
        move_ro.insertReference("call", call_reference);
      }
    }
  }
  if (!nomiel_call_uncode.isEmpty()) {
    move_ro.insertString("to", port_to_urn(nomiel_call_uncode));
  }
  return move_ro;

}

QString ContainerMoveWriter::writeDischarge(QString cargo_reference,
                                                 const ange::vessel::Vessel* vessel,
                                                 const ange::vessel::BayRowTier brt,
                                                 const ange::schedule::Call* call,
                                                 const QString& nomiel_call_uncode)
{
  ReferenceObject move_ro = buildDischarge(cargo_reference, vessel, brt, call, nomiel_call_uncode);
  d->bundle << move_ro;
  return move_ro.reference();
}

ReferenceObject ContainerMoveWriter::buildLoad(QString cargo_reference,
                                                       const ange::vessel::Vessel* vessel,
                                                       const ange::vessel::BayRowTier brt,
                                                       const ange::schedule::Call* call,
                                                       const QString& nomiel_call_uncode)
{
  ReferenceObject move_ro = d->bundle.build("move");
  move_ro.insertReference("cargo", cargo_reference);
  if (call) {
    if (d->schedule_writer) {
      QString call_reference = d->schedule_writer->reference(call);
      if (call_reference.isEmpty()) {
        qWarning("Internal inconsistency: %s is not in schedule, but still had moves for cargo %s.", call->uncode().toLocal8Bit().data(), cargo_reference.toLocal8Bit().data());
      } else {
        move_ro.insertReference("call", call_reference);
      }
    }
  }
  if (!nomiel_call_uncode.isEmpty()) {
    move_ro.insertString("from", port_to_urn(nomiel_call_uncode));
  }
  move_ro.insertString("to", urn(brt, vessel));
  return move_ro;
}

QString ContainerMoveWriter::writeLoad(QString cargo_reference,
                                            const ange::vessel::Vessel* vessel,
                                            const ange::vessel::BayRowTier brt,
                                            const ange::schedule::Call* call,
                                            const QString& nomiel_call_uncode)
{
  ReferenceObject move_ro = buildLoad(cargo_reference, vessel, brt, call, nomiel_call_uncode);
  d->bundle << move_ro;
  return move_ro.reference();
}


}
}
