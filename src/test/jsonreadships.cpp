#include <QtTest/QtTest>

#include "jsonutils.h"

class JsonReadShips : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testReadShipFiles();
    void testReadShipFiles_data();

};


void JsonReadShips::testReadShipFiles() {
    QFETCH(QString, filename);

    QString filepath = QFINDTESTDATA("smallships/" + filename);
    QVERIFY(QFile::exists(filepath));

    try {
        QVariant v = ange::stowbase::json_read(filepath);
        QVERIFY(true);
    } catch (const std::runtime_error& ex) {
        QFAIL(QString("Caught exception. parsing failed: " + filepath + " " + ex.what()).toUtf8());
    }
}


void JsonReadShips::testReadShipFiles_data()
{
    QTest::addColumn<QString>("filename");;
    QTest::newRow("lego compressed") << "lego-maersk-with-containers.sto";
    QTest::newRow("jsonobjectwithinfinity") << "jsonobjectwithinfinity.json";
  //  QTest::newRow("lego uncompressed") << "lego-maersk-with-containers.sto.uncompressed";
  //  QTest::newRow("real test vessel compressed") << "real-test-vessel.sto";
 //   QTest::newRow("real test vessel uncompressed") << "real-test-vessel.sto.uncompressed";
}
QTEST_MAIN(JsonReadShips);
#include "jsonreadships.moc"
