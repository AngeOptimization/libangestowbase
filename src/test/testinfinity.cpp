#include <QtTest/QtTest>

#include <qjson/parser.h>
#include <KF5/KArchive/KCompressionDevice>
#include <cmath>
#include <limits>
#include "jsonutils.h"

class JsonReadShips : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testInfinity();
        void testInfinityFromFile();
        void testInfinityFromFile_data();
        void testReadSmallFilesWithJsonRead();
        void testReadSmallFilesWithJsonRead_data();

};

void JsonReadShips::testInfinity() {
    QJson::Parser parser;
    parser.allowSpecialNumbers(true);

    bool ok = false;
    QVariant result = parser.parse("[Infinity]",&ok);
    QVERIFY(ok);

    QVariantList resultaslist = result.toList();
    QCOMPARE(resultaslist.size(),1);

    QVariant first = resultaslist.at(0);
    QVERIFY(std::isinf(first.toDouble()));
}

void JsonReadShips::testInfinityFromFile() {
    QFETCH(QString, filename);
    QFETCH(bool, compressed);
    QFETCH(bool, allowspecialnumbers);
    QFETCH(bool, expectedsuccess);
    QFETCH(bool, infiftrueoneiffalse);
    QString filepath = QFINDTESTDATA("testinfinity/" + filename);
    QVERIFY(QFile::exists(filepath));

    QFile file(filepath);
    QVERIFY(file.open(QIODevice::ReadOnly));

    KCompressionDevice maybecompresseddevice(&file,false, compressed ? KCompressionDevice::GZip : KCompressionDevice::None);

    QJson::Parser parser;
    parser.allowSpecialNumbers(allowspecialnumbers);

    bool ok = false;
    QVariant result;
    result = parser.parse(&maybecompresseddevice, &ok);
    QCOMPARE(expectedsuccess, ok);
    if(expectedsuccess) {
        QVariantList resultaslist = result.toList();
        QCOMPARE(1,resultaslist.size());
        QVariant resultvariantdouble = resultaslist.at(0);
        double resultdouble = resultvariantdouble.toDouble();
        if(infiftrueoneiffalse) {
            QVERIFY(std::isinf(resultdouble));
        } else {
            QCOMPARE(double(1), resultdouble);
        }

    }

}

void JsonReadShips::testInfinityFromFile_data() {
    QTest::addColumn<QString>("filename");
    QTest::addColumn<bool>("compressed");
    QTest::addColumn<bool>("allowspecialnumbers");
    QTest::addColumn<bool>("expectedsuccess");
    QTest::addColumn<bool>("infiftrueoneiffalse");

    // the commented tests should be enabled once https://github.com/flavio/qjson/issues/42 is fixed
    QTest::newRow("infinityarray uncompressed") << "infinityarray.json" << false << true << true << true;
 //   QTest::newRow("infinityarray uncompressed disallow") << "infinityarray.json" << false << false << false << true;
    QTest::newRow("infinityarray compressed") << "infinityarray.json.gz" << true << true << true << true;
 //   QTest::newRow("infinityarray compressed disallow") << "infinityarray.json.gz" << true << false << false << true;
    QTest::newRow("onearray uncompressed") << "onearray.json" << false << true << true << false;
    QTest::newRow("onearray uncompressed disallow") << "onearray.json" << false << false << true  << false;
    QTest::newRow("onearray compressed") << "onearray.json.gz" << true << true << true << false;
    QTest::newRow("onearray compressed disallow") << "onearray.json.gz" << true << false << true << false;
}

void JsonReadShips::testReadSmallFilesWithJsonRead() {
    QFETCH(QString, filename);
    QString filepath = QFINDTESTDATA("testinfinity/" + filename);
    QVERIFY(QFile::exists(filepath));

    try {
        ange::stowbase::json_read(filepath);
    } catch (const std::runtime_error& ex) {
        QFAIL(QString("Caught exception. parsing failed: " + filepath + " " + ex.what()).toUtf8());

    }
}

void JsonReadShips::testReadSmallFilesWithJsonRead_data() {
    QTest::addColumn<QString>("filename");
    QTest::newRow("infinityarray uncompressed") << "infinityarray.json";
    QTest::newRow("infinityarray compressed") << "infinityarray.json.gz";
    QTest::newRow("onearray compressed") << "onearray.json.gz";
    QTest::newRow("onearray uncompressed") << "onearray.json";
}










QTEST_MAIN(JsonReadShips);
#include "testinfinity.moc"
