
#include <QtTest/QtTest>
#include <ange/vessel/vessel.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/bayrowtier.h>

#include "../vesselreader.h"

using ange::stowbase::VesselReader;
using ange::vessel::Vessel;
using ange::vessel::BaySlice;
using ange::vessel::Stack;
using ange::vessel::Slot;
using ange::vessel::BayRowTier;

class VesselLoadTest : public QObject {
    Q_OBJECT
    public:
    private:
      Vessel* m_vessel;
    private Q_SLOTS:
        void initTestCase();
        void testVesselLoadInit();
        void testVesselJsonReader();
        void testVesselJsonReaderStackSorting();
};

void VesselLoadTest::initTestCase(){
  const QString fileName = QFINDTESTDATA("test-data/micro.xls.json");
  QFile vesselFile(fileName);
  QVERIFY(vesselFile.exists());

  vesselFile.open(QIODevice::ReadOnly);
  m_vessel = VesselReader().readVessel(&vesselFile);
}

void VesselLoadTest::testVesselLoadInit() {
  QCOMPARE(m_vessel->imoNumber(), QString("0010159"));
  QCOMPARE(m_vessel->name(), QString("Micro"));
  QCOMPARE(m_vessel->callSign(), QString("9XYZ"));
}

void VesselLoadTest::testVesselJsonReader() {

  const Slot* slot = m_vessel->slotAt(BayRowTier(0,0,0)); // does not exists
  QVERIFY(!slot);

  // over deck
  slot = m_vessel->slotAt(BayRowTier(2,0,80));  // 40' + 20'
  QVERIFY(slot);
  QCOMPARE(slot->brt().bay(), 1);  // 40' are mapped to 20 fore !!!
  QCOMPARE(slot->brt().row(), 0);
  QCOMPARE(slot->brt().tier(), 80);
  QVERIFY(slot->hasCapability(Slot::FourtyCapable));
  QVERIFY(slot->hasCapability(Slot::TwentyCapable));

  slot = m_vessel->slotAt(BayRowTier(1,0,80));  // 20' + 40'
  QVERIFY(slot);
  QCOMPARE(slot->brt().bay(), 1);
  QCOMPARE(slot->brt().row(), 0);
  QCOMPARE(slot->brt().tier(), 80);
  QVERIFY(slot->hasCapability(Slot::FourtyCapable));
  QVERIFY(slot->hasCapability(Slot::TwentyCapable));

  slot = m_vessel->slotAt(BayRowTier(2,0,82));  // 40' + 20'
  QVERIFY(slot);
  QCOMPARE(slot->brt().bay(), 3);  // 40' are mapped to 20 aft if 20 fore does not exist !!!
  QCOMPARE(slot->brt().row(), 0);
  QCOMPARE(slot->brt().tier(), 82);
  QVERIFY(!slot->hasCapability(Slot::FourtyCapable));
  QVERIFY(slot->hasCapability(Slot::TwentyCapable));

  slot = m_vessel->slotAt(BayRowTier(3,0,80));  // 20' + 40'
  QVERIFY(slot);
  QCOMPARE(slot->brt().bay(), 3);
  QCOMPARE(slot->brt().row(), 0);
  QCOMPARE(slot->brt().tier(), 80);
  QVERIFY(slot->hasCapability(Slot::FourtyCapable));
  QVERIFY(slot->hasCapability(Slot::TwentyCapable));

  slot = m_vessel->slotAt(BayRowTier(3,0,82));  // 20'
  QVERIFY(slot);
  QCOMPARE(slot->brt().bay(), 3);
  QCOMPARE(slot->brt().row(), 0);
  QCOMPARE(slot->brt().tier(), 82);
  QVERIFY(!slot->hasCapability(Slot::FourtyCapable));
  QVERIFY(slot->hasCapability(Slot::TwentyCapable));

  // below deck
  slot = m_vessel->slotAt(BayRowTier(2,0,2));  // 40' + 20'
  QVERIFY(slot);
  QCOMPARE(slot->brt().bay(), 1);  // 40' are mapped to 20 fore !!!
  QCOMPARE(slot->brt().row(), 0);
  QCOMPARE(slot->brt().tier(), 2);
  QVERIFY(slot->hasCapability(Slot::FourtyCapable));
  QVERIFY(slot->hasCapability(Slot::TwentyCapable));

  slot = m_vessel->slotAt(BayRowTier(1,0,2));  // 20' + 40'
  QVERIFY(slot);
  QCOMPARE(slot->brt().bay(), 1);  // 40' are mapped to 20 fore !!!
  QCOMPARE(slot->brt().row(), 0);
  QCOMPARE(slot->brt().tier(), 2);
  QVERIFY(slot->hasCapability(Slot::FourtyCapable));
  QVERIFY(slot->hasCapability(Slot::TwentyCapable));

  slot = m_vessel->slotAt(BayRowTier(3,0,2));  // 20' + 40'
  QVERIFY(slot);
  QCOMPARE(slot->brt().bay(), 3);  // 40' are mapped to 20 fore !!!
  QCOMPARE(slot->brt().row(), 0);
  QCOMPARE(slot->brt().tier(), 2);
  QVERIFY(slot->hasCapability(Slot::FourtyCapable));
  QVERIFY(slot->hasCapability(Slot::TwentyCapable));

  slot = m_vessel->slotAt(BayRowTier(2,0,4));  // only 40' allowed
  QVERIFY(slot);
  QCOMPARE(slot->brt().bay(), 1);  // 40' are mapped to 20 fore !!!
  QCOMPARE(slot->brt().row(), 0);
  QCOMPARE(slot->brt().tier(), 4);
  QVERIFY(slot->hasCapability(Slot::FourtyCapable));
  QVERIFY(!slot->hasCapability(Slot::TwentyCapable));

  slot = m_vessel->slotAt(BayRowTier(1,0,4));  // virtual 20', but only 40' allowed
  QVERIFY(slot);
  QCOMPARE(slot->brt().bay(), 1);
  QCOMPARE(slot->brt().row(), 0);
  QCOMPARE(slot->brt().tier(), 4);
  QVERIFY(slot->hasCapability(Slot::FourtyCapable));
  QVERIFY(!slot->hasCapability(Slot::TwentyCapable));

  slot = m_vessel->slotAt(BayRowTier(3,0,4));  // virtual 20', but only 40' allowed
  QVERIFY(slot);
  QCOMPARE(slot->brt().bay(), 3);
  QCOMPARE(slot->brt().row(), 0);
  QCOMPARE(slot->brt().tier(), 4);
  QVERIFY(slot->hasCapability(Slot::FourtyCapable));
  QVERIFY(!slot->hasCapability(Slot::TwentyCapable));
}


void VesselLoadTest::testVesselJsonReaderStackSorting() {

    Q_FOREACH(BaySlice* bayslice, m_vessel->baySlices()){
        Q_FOREACH(Stack* stack, bayslice->stacks()){
            int lowest_tier = 0;
            Q_FOREACH(Slot* slot, stack->stackSlots()) {
                QVERIFY(slot);
                qDebug() << "TEST " << slot<< " " << lowest_tier;
                QVERIFY(!(slot->brt().tier() < lowest_tier));
                lowest_tier = slot->brt().tier();
            }
        }
    }
}


QTEST_MAIN(VesselLoadTest);
#include "vessel_load_test.moc"
