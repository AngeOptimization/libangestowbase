#include <QtTest/QtTest>
#include <ange/vessel/vessel.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/bayrowtier.h>
#include <ange/vessel/stacksupport.h>
#include <ange/vessel/vesseltank.h>

#include "../vesselreader.h"

using ange::vessel::BaySlice;
using ange::vessel::Stack;
using ange::vessel::Slot;
using ange::vessel::VesselTank;
using ange::vessel::BayRowTier;

/**
 * A test to ensure that a vessel with reverse longitudinal orientation gets
 * mirrored correctly.
 */
class VesselMirrorTest : public QObject {
    Q_OBJECT
    public:
    private:
      const ange::vessel::Vessel* m_vessel;
      QList<BayRowTier> m_pureOddPositions;
    private Q_SLOTS:
        void initTestCase();
        void testVesselLoadInit();
        void testStackSupports();
        void testSlots();
        void testSpecificOddAftSlots();
        void testSpecificOddAftStackSupports();
        void testTanks();
};

void VesselMirrorTest::initTestCase(){
    const QString fileName = QFINDTESTDATA("test-data/FOWAIRET_9152260.XLS.json.gz");
    QFile vesselFile(fileName);
    QVERIFY(vesselFile.exists());
    vesselFile.open(QIODevice::ReadOnly);
    m_vessel = ange::stowbase::VesselReader().readVessel(&vesselFile);
    m_pureOddPositions = { BayRowTier(5, 7, 16),
                           BayRowTier(5, 8, 16),
                           BayRowTier(9, 9, 14),
                           BayRowTier(9, 9, 16),
                           BayRowTier(9, 10, 14),
                           BayRowTier(9, 10, 16) };
}

void VesselMirrorTest::testVesselLoadInit() {
    QCOMPARE(m_vessel->imoNumber(), QString("9152260"));
    QCOMPARE(m_vessel->name(), QString("FOWAIRET"));
}

void VesselMirrorTest::testStackSupports() {
    Q_FOREACH (const BaySlice* baySlice, m_vessel->baySlices()) {
        Q_FOREACH (const Stack* stack, baySlice->stacks()) {
            if (stack->stackSupport20Aft() && stack->stackSupport20Fore()) {
                QVERIFY2(stack->stackSupport20Aft()->bottomPos().l() < stack->stackSupport20Fore()->bottomPos().l(),
                          "Stack support longitudinal positions do not correspond to their fore/aft relation.");
            }
        } 
    }
}

void VesselMirrorTest::testSlots() {
    Q_FOREACH (const BaySlice* baySlice, m_vessel->baySlices()) {
        Q_FOREACH (const Stack* stack, baySlice->stacks()) {
            Q_FOREACH (const Slot* slot, stack->stackSlots()) {
                if (!slot->support20()) {
                    continue;
                }
                if (slot->support20() == stack->stackSupport20Aft()) {
                    QVERIFY2(slot->isAft() == true, QString("Slot %1 in aft stack support should be an aft slot.")
                                                           .arg(slot->brt().toString()).toLocal8Bit());
                } else if (slot->support20() == stack->stackSupport20Fore()) {
                    QVERIFY2(slot->isAft() == false, QString("Slot %1 in fore stack support should not be an aft slot.")
                                                           .arg(slot->brt().toString()).toLocal8Bit());
                }
            }
        } 
    }
}

void VesselMirrorTest::testSpecificOddAftSlots() {
    Q_FOREACH(const BayRowTier brt, m_pureOddPositions) {
        const Slot* oddSlot = m_vessel->slotAt(brt);
        QVERIFY2(oddSlot->isAft(), (QString("Slot %1 should be aft.").arg(brt.toString()).toLocal8Bit()));
    }
}

void VesselMirrorTest::testSpecificOddAftStackSupports() {
    Q_FOREACH(const BayRowTier brt, m_pureOddPositions) {
        const Slot* oddSlot = m_vessel->slotAt(brt);
        QVERIFY2(oddSlot->stack()->stackSupport20Fore() == 0L, "There shouldn't be any fore stack support");
        QVERIFY2(oddSlot->stack()->stackSupport20Aft(), "There should be an aft stack support");
    }
}

void VesselMirrorTest::testTanks() {    
    Q_FOREACH (const ange::vessel::VesselTank* tank, m_vessel->tanks()) {
        QVERIFY2(tank->aftEnd() <= tank->foreEnd(), "Tank's aft end should be aft of its fore end.");
    }
}

QTEST_MAIN(VesselMirrorTest);
#include "vesselmirrortest.moc"
