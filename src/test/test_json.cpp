/*
 * test_json.cpp
 *
 *  Created on: Dec 2, 2009
 *      Author: Kim Hansen
 */

#include <iostream>
#include <QFile>
#include <QVariant>
#include <qjson/parser.h>

/*
 * Test the speed of the parsing of the JSON data
 */
int main(int , char** ) {
  const QString fileName = "containers.json";
  bool ok;

  std::cout << "Parsing JSON containers ..." << std::endl;

  QFile file(fileName);
  file.open(QIODevice::ReadOnly);
  QJson::Parser parser;
  parser.parse(&file, &ok);

  if (!ok) {
    std::cout << "... parsing FAILED." << std::endl;
    return 1;
  } else {
    std::cout << "... parsing done." << std::endl;
    return 0;
  }
}
