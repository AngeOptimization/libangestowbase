find_package(Qt5Test 5.2.0 REQUIRED CONFIG)

add_compile_options(-std=c++11)

macro(make_test test_basename)
    add_executable(${test_basename} ${test_basename}.cpp)
    target_link_libraries(${test_basename}
        Stowbase
        KF5::Archive
        ${QJSON_LIBRARIES}
        Qt5::Test
    )
    add_test(${test_basename} ${test_basename})
endmacro(make_test)

make_test(jsonreadships)
make_test(testinfinity)
make_test(vessel_load_test)
make_test(vesselmirrortest)

# test_json applications
add_executable(test_json test_json.cpp)
target_link_libraries(test_json Stowbase)
