#ifndef LIBANGESTOWBASE_SCHEDULE_WRITER_H
#define LIBANGESTOWBASE_SCHEDULE_WRITER_H

#include "angestowbase_export.h"
#include <QVariant>
#include "referenceobject.h"
#include "bundle.h"
#include "jsonutils.h"

namespace ange {
namespace schedule {
class Call;
class Schedule;
}
}

namespace ange {
namespace stowbase {

class Bundle;
class schedule_writer_private_t;

class ANGESTOWBASE_EXPORT ScheduleWriter {

  public:
    ScheduleWriter();
    ~ScheduleWriter();

    /**
    * Create schedule from JSON data.
    * @param device device to read the input from.
    */
    void write(ange::stowbase::Bundle& bundle, const ange::schedule::Schedule* schedule);

    /**
     * writes uncompressed json to iodevice
     */
    void write(const ange::schedule::Schedule* schedule, QIODevice* device) const;

    /**
     * write a call to bundle
     * This function cannot be called after write_schedule has been called
     */
    void writeCall(Bundle& bundle, const schedule::Call* call);

    /**
     * Write the schedule object from the calls supplied.
     * Calling this twice is an error.
     */
    void writeSchedule(Bundle& bundle);

    /**
     * @return reference to schedule
     */
    QString reference() const;

    /**
     * @return reference to call
     */
    QString reference(const schedule::Call* call) const;

  private:
    QScopedPointer<schedule_writer_private_t> d;
};


} // stowbase
} // ange

#endif
